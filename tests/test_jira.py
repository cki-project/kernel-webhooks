"""Tests for the webhook.jira module."""
import os
import typing
from unittest import mock

from jira import JIRAError
from jira.resources import RemoteLink
from kwf_lib.common import TrackerType

from tests.helpers import KwfTestCase
from webhook import jira

if typing.TYPE_CHECKING:
    from jira.resources import ResilientSession


class TestJira(KwfTestCase):
    """Tests for the webhook.jira module functions."""

    def test_update_issue(self) -> None:
        """Calls the update() method of the given jira.resources.Issue with the given values."""
        # Really does nothing in the development case.
        cki_env = {'CKI_DEPLOYMENT_ENVIRONMENT': 'development'}
        with mock.patch.dict(os.environ, cki_env, clear=True):
            mock_issue = mock.Mock(spec_set=['key', 'update'], key='TEST-123')

            jira.update_issue(mock_issue, fields={'a': 'b'}, update={})
            mock_issue.update.assert_not_called()

        # Calls Issue.update() when not in the development env.
        cki_env = {'CKI_DEPLOYMENT_ENVIRONMENT': 'production'}
        with mock.patch.dict(os.environ, cki_env, clear=True):
            mock_issue = mock.Mock(spec_set=['key', 'update'], key='TEST-123')

            jira.update_issue(mock_issue, fields={'a': 'b'}, update={})
            mock_issue.update.assert_called_once_with(fields={'a': 'b'}, update={})

    def test_kwf_jql_filter(self) -> None:
        """Returns the expected JQL query string to filter in KWF related issues."""
        expected_jql = 'project in (RHEL) AND issuetype in (Task, Bug, Story, Vulnerability, Weakness) AND component in componentMatch("^(kernel|kernel-rt|kernel-automotive)($| / .*$)")'  # noqa: E501
        self.assertEqual(
            jira.KWF_JQL_FILTER,
            expected_jql
        )

        self.assertEqual(
            jira.KWF_JQL_FILTER,
            expected_jql
        )

    def test_jql_query(self) -> None:
        """Calls JIRA.search_issues() with the given jql string and returns the results."""
        mock_jira = mock.Mock(spec_set=['search_issues'])
        mock_jira.search_issues.return_value = [mock.Mock(spec_set=['key'], key='TEST-456')]

        self.assertEqual(
            jira.jql_query(mock_jira, 'query_string'),
            mock_jira.search_issues.return_value
        )

        mock_jira.search_issues.assert_called_once_with('query_string')

        # Logs a warning and returns an empty list on JIRAError exception.
        mock_jira.reset_mock(return_value=True, side_effect=True)
        mock_jira.search_issues.side_effect = JIRAError(status_code=123)

        self.assertEqual(
            jira.jql_query(mock_jira, 'query_string'),
            []
        )


def make_remote_link(raw_link: dict, session: 'ResilientSession | None' = None) -> RemoteLink:
    """Construct a RemoteLink and return it for testing."""
    return RemoteLink(options={}, raw=raw_link, session=session)


class TestDeleteResource(KwfTestCase):
    """Tests for the webhook.jira.delete_resource() function."""

    def setUp(self) -> None:
        """Provide a RemoteLink resource with mocked delete() method."""
        super().setUp()

        test_remote_link = make_remote_link({
            'application': {},
            'id': 1738608,
            'object': {'title': 'Test RemoteLink', 'url': 'https://example.com'},
            'self': 'https://issues.redhat.com/rest/api/2/issue/RHEL-123/remotelink/123456'
        })
        test_remote_link.delete = mock.Mock()

        self.test_remote_link = test_remote_link

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=False))
    def test_delete_resource_no_prod(self) -> None:
        """Returns True without having called Resource.delete() when not production/staging."""
        self.assertFalse(jira.is_production_or_staging())
        self.assertTrue(jira.delete_resource(self.test_remote_link))
        self.test_remote_link.delete.assert_not_called()

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=True))
    def test_delete_resource_prod_ok(self) -> None:
        """Returns True and calls Resource.delete() when in production/staging."""
        self.assertTrue(jira.is_production_or_staging())
        self.assertTrue(jira.delete_resource(self.test_remote_link))
        self.test_remote_link.delete.assert_called_once()

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=True))
    def test_delete_resource_prod_404_okay(self) -> None:
        """Returns True and ignores 404 on Resource.delete() when in production/staging."""
        self.assertTrue(jira.is_production_or_staging())
        self.test_remote_link.delete.side_effect = JIRAError(status_code=404)

        self.assertFalse(jira.delete_resource(self.test_remote_link))
        self.test_remote_link.delete.assert_called_once()

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=True))
    def test_delete_resource_prod_404_raise(self) -> None:
        """Raises on 404 on Resource.delete() when raise_on_404 is set."""
        self.assertTrue(jira.is_production_or_staging())
        self.test_remote_link.delete.side_effect = JIRAError(status_code=404)

        with self.assertRaises(JIRAError):
            jira.delete_resource(self.test_remote_link, raise_on_404=True)
            self.test_remote_link.delete.assert_called_once()

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=True))
    def test_delete_resource_prod_unexpected_status(self) -> None:
        """Raises on any status code other than 404."""
        self.assertTrue(jira.is_production_or_staging())
        self.test_remote_link.delete.side_effect = JIRAError(status_code=401)

        with self.assertRaises(JIRAError):
            jira.delete_resource(self.test_remote_link)
            self.test_remote_link.delete.assert_called_once()


class TestUpdateResource(KwfTestCase):
    """Tests for the webhook.jira.update_resource() function."""

    def setUp(self) -> None:
        """Provide a RemoteLink resource with mocked update() method."""
        super().setUp()

        test_remote_link = make_remote_link({
            'application': {},
            'id': 1738608,
            'object': {'title': 'Test RemoteLink', 'url': 'https://example.com'},
            'self': 'https://issues.redhat.com/rest/api/2/issue/RHEL-123/remotelink/123456'
        })
        test_remote_link.update = mock.Mock()

        self.test_remote_link = test_remote_link

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=False))
    def test_update_resource_no_prod(self) -> None:
        """Returns True without having called Resource.update() when not production/staging."""
        self.assertFalse(jira.is_production_or_staging())
        jira.update_resource(self.test_remote_link)
        self.test_remote_link.update.assert_not_called()

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=True))
    def test_update_resource_prod_ok(self) -> None:
        """Returns True and calls Resource.update() when in production/staging."""
        self.assertTrue(jira.is_production_or_staging())

        kwargs = {'object': {'a': 1, 'b': True}}

        jira.update_resource(self.test_remote_link, **kwargs)
        self.test_remote_link.update.assert_called_once_with(**kwargs)


class TestGetIssues(KwfTestCase):
    """Tests for the webhook.jira.get_issues() function."""

    def setUp(self) -> None:
        """Mock a JIRA instance and jql_query."""
        super().setUp()

        mock_jira = mock.Mock(spec_set=['client_info'])
        mock_jira.client_info.return_value = 'https://issues.example.com'
        self.mock_jira = mock_jira

        mock_jql_query = mock.patch('webhook.jira.jql_query', mock.Mock())
        self.addCleanup(mock_jql_query.stop)
        self.mock_jql_query = mock_jql_query.start()

    def test_invalid_input(self) -> None:
        """Raises a ValueError exception if neither issue_ids nor cve_ids are given."""
        with self.assertRaises(ValueError):
            jira.get_issues(self.mock_jira)

        with self.assertRaises(ValueError):
            jira.get_issues(self.mock_jira, issue_ids=[])

        with self.assertRaises(ValueError):
            jira.get_issues(self.mock_jira, cve_ids=[])

        with self.assertRaises(ValueError):
            jira.get_issues(self.mock_jira, issue_ids=[], cve_ids=[])

        self.mock_jql_query.assert_not_called()

    def run_test(self, expected_jql: str, **kwargs) -> None:
        """Call webhook.get_isues with the given params and confirm jql_query has the expected."""
        self.assertEqual(
            jira.get_issues(self.mock_jira, **kwargs),
            self.mock_jql_query.return_value
        )

        self.mock_jql_query.assert_called_once_with(self.mock_jira, expected_jql)

    def test_one_issue(self) -> None:
        """Builds the expected query string and passes it to jql_query."""
        params = {
            'issue_ids': ['TEST-555']
        }
        expected_jql = 'key=TEST-555'
        self.run_test(expected_jql, **params)

    def test_multiple_issues(self) -> None:
        """Builds the expected query string and passes it to jql_query."""
        params = {
            'issue_ids': ['RHEL-123', 'TEST-555', 'CS-9999']
        }
        expected_jql = 'key=RHEL-123 OR key=TEST-555 OR key=CS-9999'
        self.run_test(expected_jql, **params)

        # filter_kwf=True should not affect the result when only issue_ids are given.
        self.mock_jql_query.reset_mock()
        params['filter_kwf'] = True
        self.run_test(expected_jql, **params)

    def test_multiple_issues_with_linked(self) -> None:
        """Builds the expected query string and passes it to jql_query."""
        params = {
            'issue_ids': ['RHEL-123', 'TEST-555', 'CS-9999'],
            'with_linked': True
        }
        expected_jql = 'key=RHEL-123 OR key=TEST-555 OR key=CS-9999 OR ( (issue in linkedIssues(RHEL-123) OR issue in linkedIssues(TEST-555) OR issue in linkedIssues(CS-9999)))'  # noqa: E501
        self.run_test(expected_jql, **params)

    def test_one_cve_id(self) -> None:
        """Builds the expected query string and passes it to jql_query."""
        params = {
            'cve_ids': ['CVE-2024-9876']
        }
        expected_jql = ' (labels in (CVE-2024-9876) OR "CVE ID" ~ CVE-2024-9876)'
        self.run_test(expected_jql, **params)

    def test_multiple_cve_ids(self) -> None:
        """Builds the expected query string and passes it to jql_query."""
        params = {
            'cve_ids': ['CVE-2024-9876', 'CVE-1999-7777', 'CVE-2100-51633']
        }
        expected_jql = ' (labels in (CVE-2024-9876, CVE-1999-7777, CVE-2100-51633) OR "CVE ID" ~ CVE-2024-9876 OR "CVE ID" ~ CVE-1999-7777 OR "CVE ID" ~ CVE-2100-51633)'  # noqa: E501
        self.run_test(expected_jql, **params)

    def test_cve_ids_with_filter_kwf(self) -> None:
        """Builds the expected query string and passes it to jql_query."""
        params = {
            'cve_ids': ['CVE-2024-9876', 'CVE-1999-7777', 'CVE-2100-51633'],
            'filter_kwf': True
        }

        expected_jql = f'({jira.KWF_JQL_FILTER}) AND (labels in (CVE-2024-9876, CVE-1999-7777, CVE-2100-51633) OR "CVE ID" ~ CVE-2024-9876 OR "CVE ID" ~ CVE-1999-7777 OR "CVE ID" ~ CVE-2100-51633)'  # noqa: E501
        self.run_test(expected_jql, **params)

    def test_issue_and_cves_and_linked_and_kwf_filter(self) -> None:
        """Builds the expected query string and passes it to jql_query."""
        params = {
            'issue_ids': ['RHEL-555', 'TEST-999'],
            'cve_ids': ['CVE-2024-9876', 'CVE-1999-7777', 'CVE-2100-51633'],
            'with_linked': True,
            'filter_kwf': True
        }

        expected_jql = f'key=RHEL-555 OR key=TEST-999 OR (({jira.KWF_JQL_FILTER}) AND (issue in linkedIssues(RHEL-555) OR issue in linkedIssues(TEST-999) OR labels in (CVE-2024-9876, CVE-1999-7777, CVE-2100-51633) OR "CVE ID" ~ CVE-2024-9876 OR "CVE ID" ~ CVE-1999-7777 OR "CVE ID" ~ CVE-2100-51633))'  # noqa: E501
        self.run_test(expected_jql, **params)


class TestCreateIssue(KwfTestCase):
    """Tests for the webhook.jira.create_issue() function."""

    def setUp(self) -> None:
        """Mock a JIRA instance."""
        super().setUp()

        self.mock_jira = mock.Mock(spec_set=['create_issue'], return_value=None)

        self.params = {
            'summary': 'A lovely new issue',
            'description': 'This describes this new issue in magnificent detail.',
            'fixversion': 'v6.14',
            'component': 'kernel',
            'issuetype': TrackerType.TASK,
            'project': 'LINUX'
        }

        self.expected_fields = {
            'project': {'key': 'LINUX'},
            'summary': 'A lovely new issue',
            'description': 'This describes this new issue in magnificent detail.',
            'issuetype': {'name': 'Task'},
            'fixVersions': [{'name': 'v6.14'}],
            'components': [{'name': 'kernel'}]
        }

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=False))
    def test_create_issue_no_prod(self) -> None:
        """Returns True without having called JIRA.create_issue() when not production/staging.
        The function should also return None in this case."""
        self.assertFalse(jira.is_production_or_staging())
        issue = jira.create_issue(self.mock_jira, **self.params)
        self.mock_jira.create_issue.assert_not_called()
        self.assertIsNone(issue)

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=True))
    def test_create_issue_prod_ok(self) -> None:
        """Returns True and calls JIRA.create_issue() when in production/staging."""
        self.assertTrue(jira.is_production_or_staging())
        jira.create_issue(self.mock_jira, **self.params)
        self.mock_jira.create_issue.assert_called_once_with(fields=self.expected_fields)

"""Tests for report_generator."""
from copy import deepcopy
from datetime import timedelta
import os
from unittest import mock

from cki_lib import misc
from cki_lib import owners
from cki_lib import yaml

from tests.helpers import KwfTestCase
from webhook import defs
from webhook.rhissue import RHIssue
from webhook.utils import report_generator


class TestReportGenerator(KwfTestCase):
    """Tests for the various helper functions."""

    MOCK_MR = {'iid': '66',
               'title': 'This is the title of my MR',
               'webUrl': 'https://gitlab.com/foo/bar/blah/-/merge_requests/66',
               'project': {'name': 'blah', 'fullPath': 'foo/bar/blah'},
               'targetBranch': 'main',
               'author': {'username': 'shadowman',
                          'name': 'Shadow Man',
                          'email': 'shadowman@redhat.com'},
               'labels': {'nodes': [{'title': 'readyForMerge',
                                     'description': 'This MR is ready for merge'}]},
               'updatedAt': '2023-06-28 13:41:25.012179',
               'createdAt': '2023-06-27 13:41:25.012179',
               'report_mr': mock.Mock(all_rhissue_ids={'RHEL-101'}),
               'draft': False}

    MOCK_MR2 = {'iid': '67',
                'title': 'This is the title of my 2nd MR',
                'webUrl': 'https://gitlab.com/foo/bar/blah/-/merge_requests/67',
                'project': {'name': 'blah', 'fullPath': 'foo/bar/blah'},
                'targetBranch': 'main',
                'author': {'username': 'shadowman',
                           'name': 'Shadow Man',
                           'email': 'shadowman@redhat.com'},
                'labels': {'nodes': [{'title': 'readyForMerge',
                                      'description': 'This MR is ready for merge'}]},
                'updatedAt': '2023-06-29 13:41:25.012179',
                'createdAt': '2023-06-27 13:41:25.012179',
                'report_mr': mock.Mock(all_rhissue_ids={'RHEL-102'}),
                'draft': False}

    MOCK_MR3 = {'iid': '68',
                'title': 'This is the title of my 3rd MR',
                'webUrl': 'https://gitlab.com/foo/bar/blah/-/merge_requests/68',
                'project': {'name': 'blah', 'fullPath': 'foo/bar/blah'},
                'targetBranch': 'main',
                'author': {'username': None,
                           'name': 'Shadow Man',
                           'email': 'shadowman@redhat.com'},
                'labels': {'nodes': [{'title': 'readyForMerge',
                                      'description': 'This MR is ready for merge'}]},
                'updatedAt': '2023-06-28 13:41:25.012179',
                'createdAt': '2023-06-27 13:41:25.012179',
                'report_mr': mock.Mock(all_rhissue_ids={'RHEL-103'}),
                'draft': False}

    MOCK_MR_LIST = {'foo/bar/blah': [MOCK_MR, MOCK_MR2]}

    GQL_MRS = {'project':
               {'id': 'gid://gitlab/Project/1234',
                'mergeRequests':
                {'pageInfo': {'hasNextPage': False, 'endCursor': 'eyJjc'},
                 'nodes': [MOCK_MR, MOCK_MR2]}}}

    MOCK_LDAP = [('uid=shadowman,ou=users,dc=redhat,dc=com',
                  {'memberOf':
                   [b'cn=Employee,ou=userClass,dc=redhat,dc=com',
                    b'cn=rhel-sst-kernel-maintainers,ou=adhoc,ou=managedGroups,dc=redhat,dc=com'],
                   'rhatPrimaryMail': [b'shadowman@redhat.com']})]

    MOCK_LDAP2 = [('uid=shadowman,ou=users,dc=redhat,dc=com',
                   {'memberOf':
                    [b'cn=Employee,ou=userClass,dc=redhat,dc=com',
                     b'cn=foo,ou=adhoc,ou=managedGroups,dc=redhat,dc=com'],
                    'rhatPrimaryMail': [b'shadowman@redhat.com']})]

    @mock.patch('webhook.utils.report_generator.ReportMR.new')
    def test_get_open_mrs(self, mock_new_report_mr):
        mock_session = mock.Mock()
        mock_session.graphql.check_query_results.return_value = self.GQL_MRS
        namespace = 'foo/bar/blah'
        report_generator.get_open_mrs(mock_session, namespace)
        mock_new_report_mr.assert_called()
        self.assertEqual(mock_new_report_mr.call_count, 2)

    @mock.patch('webhook.utils.report_generator.initialize')
    def test_get_author_sst_from_ldap(self, mock_ldap):
        author = {'name': 'Shadow Man', 'username': 'shadowman', 'email': 'shadowman@redhat.com'}
        args = mock.Mock(ldap_server="ldap.redhat.com",
                         base_dn="ou=users,dc=redhat,dc=com",
                         ldap_attrs="memberOf")
        mock_connection = mock.Mock()
        mock_ldap.return_value = mock_connection
        mock_connection.search_s.return_value = deepcopy(self.MOCK_LDAP)
        ret = report_generator.get_author_sst_from_ldap(args, author)
        self.assertEqual(ret, 'rhel-sst-kernel-maintainers')

        # Have email, found in ldap, but cannot find a valid sst
        mock_connection.search_s.return_value = deepcopy(self.MOCK_LDAP2)
        ret = report_generator.get_author_sst_from_ldap(args, author)
        self.assertEqual(ret, report_generator.NULL_SST)

        # Have email, can't find in ldap
        mock_connection.search_s.return_value = ''
        ret = report_generator.get_author_sst_from_ldap(args, author)
        self.assertEqual(ret, 'EMAIL-NOT-FOUND')

        # No email, unknown SST
        author = {'name': 'Shadow Man', 'username': 'shadowman', 'email': ''}
        ret = report_generator.get_author_sst_from_ldap(args, author)
        self.assertEqual(ret, 'UNKNOWN-SST')

    @mock.patch('webhook.utils.report_generator.get_author_sst_from_ldap',
                mock.Mock(return_value='rhel-sst-misc'))
    def test_get_author_info(self):
        author = {'name': 'Shadow Man', 'username': 'shadowman', 'email': 'shadowman@redhat.com'}
        authors = {}
        exp = {'shadowman': author}
        exp['shadowman']['sst'] = 'rhel-sst-misc'
        # Standard case
        ret = report_generator.get_author_info(True, self.MOCK_MR, authors)
        self.assertEqual(ret, exp)
        # User already in authors
        authors = {'shadowman': author}
        with self.assertLogs('cki.webhook.utils.report_generator', level='DEBUG') as logs:
            ret = report_generator.get_author_info(True, self.MOCK_MR, authors)
            self.assertEqual(ret, {})
            self.assertIn("User shadowman already in authors", logs.output[-1])
        # No username found
        authors = {}
        with self.assertLogs('cki.webhook.utils.report_generator', level='INFO') as logs:
            ret = report_generator.get_author_info(True, self.MOCK_MR3, authors)
            self.assertEqual(ret, {})
            self.assertIn("No username for mreq 68", logs.output[-1])

    def test_format_mr_entry(self):
        mr_data = self.MOCK_MR
        exp = ("  MR 66: This is the title of my MR (@shadowman)\n"
               "    https://gitlab.com/foo/bar/blah/-/merge_requests/66\n"
               "    Project: blah, Target Branch: main\n")
        ret = report_generator.format_mr_entry(mr_data)
        self.assertEqual(ret, exp)

    def test_parse_labels(self):
        mreq_labels = [{'title': 'foo', 'description': 'This is label foo'}]
        label_info = report_generator.parse_labels(mreq_labels)
        self.assertEqual("\n", label_info)
        mreq_labels = [{'title': f'{defs.READY_FOR_QA_LABEL}', 'description': 'Ready for QA'},
                       {'title': 'Signoff::NeedsReview', 'description': 'Signoff Needs review'}]
        label_info = report_generator.parse_labels(mreq_labels)
        self.assertEqual("    - Signoff Needs review\n\n", label_info)

    def test_get_subsystems(self):
        mreq_labels = [{'title': 'Subsystem:foo', 'description': 'Subsystem foo label'}]
        ret = report_generator.get_subsystems(mreq_labels)
        self.assertEqual(ret, ['foo'])

    def test_format_subsystem_entries(self):
        section = 'conflicts'
        report = ("  MR 66: This is the title of my MR (@shadowman)\n"
                  "    https://gitlab.com/foo/bar/blah/-/merge_requests/66\n"
                  "    Project: blah, Target Branch: main\n")
        reports = {}
        reports[section] = {'rhel-sst-foo': report}
        ret = report_generator.format_subsystem_entries(reports, section)
        self.assertEqual(f"*** foo conflicts ***\n{report}\n", ret)

    def test_section_info_builder(self):
        mock_args = mock.Mock(stale='15', draft_stale='90')
        report_list = ['stale_dev', 'stale_draft', 'stale_qa', 'stale_km', 'conflicts', 'warnings',
                       'closed_issues', 'wrong_stream']
        info = report_generator.section_info_builder(mock_args, report_list)
        self.assertEqual(info['stale_dev'],
                         "5. MRs stalled in development for 15+ days (stale_dev)\n")
        self.assertEqual(info['stale_draft'],
                         "8. MR drafts stalled for 90+ days (stale_draft)\n")

    def test_format_section_header(self):
        text = "yeehaw\n"
        exp = "-------\nyeehaw\n-------\n"
        ret = report_generator.format_section_header(text)
        self.assertEqual(ret, exp)

    def test_format_report_body(self):
        mock_args = mock.Mock(conflicts=True, stale='15', draft_stale='90')
        report_list = ['stale_dev', 'stale_draft', 'stale_qa', 'stale_km', 'conflicts', 'warnings',
                       'closed_issues', 'wrong_stream']
        info = report_generator.section_info_builder(mock_args, report_list)
        report = ("  MR 66: This is the title of my MR (@shadowman)\n"
                  "    https://gitlab.com/foo/bar/blah/-/merge_requests/66\n"
                  "    Project: blah, Target Branch: main\n")
        reports = {}
        for section in report_list:
            reports[section] = {'rhel-sst-foo': report}
        ret = report_generator.format_report_body(mock_args, reports, info)
        self.assertIn("Table of Contents", ret)
        self.assertIn("Long Description (short_tag)", ret)
        self.assertIn("(NOTE: Empty sections will NOT be shown in the report body)", ret)
        self.assertIn("*** foo conflicts ***", ret)
        self.assertIn("*** foo stale_draft ***", ret)

    @mock.patch('smtplib.SMTP')
    def test_send_emailed_report(self, mock_smtp):
        mock_args = mock.Mock(email=['to@example.com'], from_address='from@example.com',
                              smtp_url='smtp.example.com', cc_sst_lists=True)
        body = 'This is an email body'
        cc_list = ['nobody@example.com']
        report_generator.send_emailed_report(mock_args, body, cc_list)
        mock_smtp.assert_called_once()

    def test_check_age_of_mr(self):
        args = mock.Mock(stale=15, draft_stale=90)
        updated_at = str(misc.now_tz_utc() - timedelta(days=91))
        ret = report_generator.check_age_of_mr(args, updated_at)
        self.assertEqual(ret, 2)
        updated_at = str(misc.now_tz_utc() - timedelta(days=16))
        ret = report_generator.check_age_of_mr(args, updated_at)
        self.assertEqual(ret, 1)
        updated_at = str(misc.now_tz_utc() - timedelta(days=1))
        ret = report_generator.check_age_of_mr(args, updated_at)
        self.assertEqual(ret, 0)

    def test_find_last_relevant_update(self):
        mreq = {}
        mock_bot_author = mock.Mock(username='cki-kwf-bot')
        mock_human_author = mock.Mock(username='shadowman')
        mock_bot_note = mock.Mock(body="whatever", author=mock_bot_author, updatedAt="2222")
        mock_human_note = mock.Mock(body="I like this", author=mock_human_author, updatedAt="1234")
        mock_bot_discussion = mock.Mock(notes=[mock_bot_note])
        mock_human_discussion = mock.Mock(notes=[mock_human_note])
        mock_report_mr = mock.Mock()
        mock_report_mr.discussions = [mock_bot_discussion, mock_human_discussion]
        mock_report_mr.system_discussions = []
        mreq['createdAt'] = '0'
        mreq['report_mr'] = mock_report_mr
        retval = report_generator.find_last_relevant_update(mreq)
        self.assertEqual(retval, "1234")

        mock_bot_note = mock.Mock(body="approved this merge request", author=mock_human_author,
                                  updatedAt="7890")
        mock_human_discussion = mock.Mock(notes=[mock_human_note])
        mock_bot_note = mock.Mock(body="approved this merge request", author=mock_bot_author,
                                  updatedAt="3456")
        mock_bot_discussion = mock.Mock(notes=[mock_bot_note])
        mock_report_mr.system_discussions = [mock_bot_discussion, mock_human_discussion]
        retval = report_generator.find_last_relevant_update(mreq)
        self.assertEqual(retval, "3456")

    @mock.patch('webhook.utils.report_generator.ReportMR', mock.Mock())
    @mock.patch('webhook.utils.report_generator.get_open_mrs')
    def test_main_no_open_mrs(self, mock_mrs):
        mock_args = mock.Mock(groups=['redhat/rhel/src/kernel'],
                              projects=['cki-project/kernel-ark'])
        mock_mrs.return_value = {}
        self.response_gl_auth()
        self.response_gql_user_data()
        with self.assertLogs('cki.webhook.utils.report_generator', level='INFO') as logs:
            report_generator.main(mock_args)
            self.assertIn("Finding open MRs", logs.output[-2])
            self.assertIn("No open MRs to process.", logs.output[-1])

    def test_extract_subsystems_data(self):
        owners_yaml = ("subsystems:\n"
                       " - subsystem: Foo Subsystem\n"
                       "   labels:\n"
                       "     name: foo\n"
                       "   devel-sst:\n"
                       "     - rhel-sst-foo\n"
                       "   mailingList: mailinglist@redhat.com\n"
                       "   requiredApproval: false\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "       gluser: user1\n"
                       "   reviewers:\n"
                       "     - name: User 2\n"
                       "       email: user2@redhat.com\n"
                       "       gluser: user2\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - redhat/\n"
                       "          - Documentation/\n")
        owners_parser = owners.Parser(yaml.load(contents=owners_yaml))
        mreq_labels = [{'title': 'Subsystem:foo', 'description': 'Subsystem foo label'}]
        ssts, cc_list = report_generator.extract_subsystems_data(owners_parser, mreq_labels)
        self.assertEqual(ssts, ['foo'])
        self.assertEqual(cc_list, ['mailinglist@redhat.com'])

    def test_get_qa_contact_info(self):
        mock_qa_contact = mock.Mock()
        mock_qa_contact.displayName = "Code Tester"
        mock_qa_contact.emailAddress = "codetester@example.com"
        mock_rhi = mock.Mock()
        mock_rhi.qa_contact = mock_qa_contact
        mock_rmr = mock.Mock()
        mock_rmr.rhissues = [mock_rhi]
        contact = report_generator.get_qa_contact_info(mock_rmr)
        self.assertEqual(contact, "Code Tester <codetester@example.com>")

    def test_check_waiting_on_dev(self):
        mreq = {'extra_info': ''}
        issue = self.make_jira_issue("RHEL-101")
        rhi = RHIssue.new_from_ji(ji=issue, mrs=None)
        rhi.failed_tests = []
        self.assertFalse(report_generator.check_waiting_on_dev(mreq, [rhi]))
        rhi.failed_tests = ["DevApproved"]
        self.assertTrue(report_generator.check_waiting_on_dev(mreq, [rhi]))
        rhi.failed_tests = ["AssigneeIsNotBot"]
        self.assertTrue(report_generator.check_waiting_on_dev(mreq, [rhi]))

    @mock.patch('webhook.utils.report_generator.PrelimTestingPass')
    def test_check_waiting_on_qa(self, mock_ptp):
        mock_ptp.return_value = True
        mock_rmr = mock.Mock()
        mock_rmr.ready_for_qa = False
        mreq = {'extra_info': '', 'report_mr': mock_rmr}
        issue = self.make_jira_issue("RHEL-101")
        rhi = RHIssue.new_from_ji(ji=issue, mrs=None)
        rhi.failed_tests = []
        self.assertFalse(report_generator.check_waiting_on_qa(mreq, [rhi]))
        rhi.failed_tests = ["QEApproved"]
        self.assertTrue(report_generator.check_waiting_on_qa(mreq, [rhi]))
        rhi.failed_tests = []
        mock_ptp.return_value = False
        self.assertTrue(report_generator.check_waiting_on_qa(mreq, [rhi]))

    @mock.patch('webhook.utils.report_generator.check_waiting_on_qa')
    @mock.patch('webhook.utils.report_generator.check_waiting_on_dev')
    @mock.patch('webhook.utils.report_generator.get_qa_contact_info')
    @mock.patch('webhook.utils.report_generator.parse_labels')
    @mock.patch('webhook.utils.report_generator.check_age_of_mr')
    def test_update_report_categories(self, mock_age, mock_pl, mock_qa, mock_wod, mock_woqa):
        args = mock.Mock()
        mock_age.return_value = 2
        mock_qa.return_value = 'Mister Tester <mrtester@example.com>'
        report_list = ['stale_dev', 'stale_draft', 'stale_qa', 'stale_km', 'conflicts',
                       'warnings', 'closed_issues', 'wrong_stream']
        reports = {}
        author_sst = 'rhel-sst-foo'
        for section in report_list:
            reports[section] = {author_sst: 'report info\n'}
        mock_wod.return_value = False
        mock_woqa.return_value = False

        # Waiting on kernel maintainer to merge
        mock_pl.return_value = '    some info\n'
        mreq = self.MOCK_MR
        mock_report_mr = mock.Mock()
        mock_report_mr.all_rhissue_ids = {'RHEL-101'}
        issue = self.make_jira_issue("RHEL-101")
        rhi = RHIssue.new_from_ji(ji=issue, mrs=None)
        rhi.failed_tests = []
        mock_report_mr.rhissues_with_scopes = [rhi]
        mock_report_mr.merge_conflict = False
        mreq['report_mr'] = mock_report_mr
        mreq['mr_info'] = '  mr info\n'
        mreq['extra_info'] = '    extra info\n'
        mreq['updatedAt'] = str(misc.now_tz_utc())
        report_generator.update_report_categories(args, reports, mreq, author_sst)
        combined = ('report info\n'
                    '  mr info\n'
                    '    Referenced jira issues: RHEL-101\n'
                    '    extra info\n'
                    '    some info\n')
        self.assertEqual(reports['stale_km'][author_sst], combined)

        # Jira issue is closed
        rhi.failed_tests = ["JIisNotClosed"]
        mreq['mr_info'] = '  mr info\n'
        mreq['extra_info'] = '    extra info\n'
        report_generator.update_report_categories(args, reports, mreq, author_sst)
        closed = ('report info\n'
                  '  mr info\n'
                  '    Referenced jira issues: RHEL-101\n'
                  '    extra info\n'
                  '    - This JIRA Issue\'s status is CLOSED.  '
                  'Please check that the JIRA information is correct.\n\n')
        self.assertEqual(reports['closed_issues'][author_sst], closed)

        # MR is against the wrong stream
        rhi.failed_tests = ["CentOSZStream"]
        mreq['mr_info'] = '  mr info\n'
        mreq['extra_info'] = '    extra info\n'
        report_generator.update_report_categories(args, reports, mreq, author_sst)
        centos = ('report info\n'
                  '  mr info\n'
                  '    Referenced jira issues: RHEL-101\n'
                  '    extra info\n'
                  '    - This JIRA Issue targets a zstream release but this MR exists in '
                  'the Centos Stream project.  Centos Stream Issues are expected to target '
                  'ystream.  This MR may need to be recreated in the RHEL9 project; please '
                  'contact a maintainer for further assistance.\n'
                  '    some info\n')
        self.assertEqual(reports['wrong_stream'][author_sst], centos)

        reports['wrong_stream'] = {author_sst: 'report info\n'}
        rhi.failed_tests = ["TargetReleaseSet"]
        mreq['mr_info'] = '  mr info\n'
        mreq['extra_info'] = '    extra info\n'
        report_generator.update_report_categories(args, reports, mreq, author_sst)
        norelease = ('report info\n'
                     '  mr info\n'
                     '    Referenced jira issues: RHEL-101\n'
                     '    extra info\n'
                     '    - This JIRA Issue does not have a valid Fix Version set.\n'
                     '    some info\n')
        self.assertEqual(reports['wrong_stream'][author_sst], norelease)

        combined_qa = ('report info\n'
                       '  mr info\n'
                       '    Referenced jira issues: RHEL-101\n'
                       '    QA Contact(s): Mister Tester <mrtester@example.com>\n'
                       '    extra info\n'
                       '    some info\n')
        # Waiting on QA to test
        mreq['mr_info'] = '  mr info\n'
        mreq['extra_info'] = '    extra info\n'
        mock_pl.return_value = '    some info\n'
        rhi.failed_tests = []
        mock_report_mr.ready_for_merge = False
        mock_woqa.return_value = True
        report_generator.update_report_categories(args, reports, mreq, author_sst)
        self.assertEqual(reports['stale_qa'][author_sst], combined_qa)

        # Waiting on developers, with merge conflicts
        mreq['mr_info'] = '  mr info\n'
        mreq['extra_info'] = '    extra info\n'
        mock_pl.return_value = '    some info\n'
        mock_report_mr.ready_for_qa = False
        mock_woqa.return_value = False
        mock_wod.return_value = True
        mock_report_mr.merge_conflict = True
        report_generator.update_report_categories(args, reports, mreq, author_sst)
        self.assertEqual(reports['conflicts'][author_sst],
                         'report info\n  mr info\n    Referenced jira issues: RHEL-101\n\n')

        # Waiting on developers, with merge warnings
        mreq['mr_info'] = '  mr info\n'
        mreq['extra_info'] = '    extra info\n'
        mock_pl.return_value = '    some info\n'
        mock_report_mr.merge_conflict = False
        mock_report_mr.merge_warning = True
        for section in report_list:
            reports[section] = {author_sst: 'report info\n'}
        report_generator.update_report_categories(args, reports, mreq, author_sst)
        self.assertEqual(reports['warnings'][author_sst],
                         'report info\n  mr info\n    Referenced jira issues: RHEL-101\n\n')

        # Stale draft
        mreq['mr_info'] = '  mr info\n'
        mreq['extra_info'] = '    extra info\n'
        mreq['draft'] = True
        mock_pl.return_value = '    some info\n'
        report_generator.update_report_categories(args, reports, mreq, author_sst)
        self.assertEqual(reports['stale_draft'][author_sst], combined)

    def test_extend_reports_categories(self):
        report_list = ['stale_dev', 'stale_draft', 'stale_qa', 'stale_km', 'conflicts', 'warnings']
        reports = dict.fromkeys(report_list, '')
        new_sst = 'cli'
        report_generator.build_reports_categories(reports, report_list, {})
        report_generator.extend_reports_categories(reports, report_list, new_sst)
        for category in report_list:
            self.assertIn(new_sst, reports[category])

    def test_build_reports_categories(self):
        report_list = ['stale_dev', 'stale_draft', 'stale_qa', 'stale_km', 'conflicts', 'warnings']
        reports = dict.fromkeys(report_list, '')
        authors = {'shadowman': {'username': 'shadowman', 'name': 'Shadow Man',
                                 'email': 'shadowman@redhat.com', 'sst': 'rhel-sst-foo'}}
        ret = report_generator.build_reports_categories(reports, report_list, authors)
        self.assertEqual(reports['conflicts']['rhel-sst-foo'], '')
        self.assertEqual(['rhel-sst-foo'], ret)

    @mock.patch('webhook.utils.report_generator.update_report_categories')
    @mock.patch('webhook.utils.report_generator.find_last_relevant_update')
    @mock.patch('webhook.utils.report_generator.extract_subsystems_data')
    @mock.patch('webhook.utils.report_generator.parse_labels')
    @mock.patch('webhook.utils.report_generator.check_age_of_mr')
    def test_examine_mrs(self, mock_age, mock_pl, mock_sstdata, mock_last_update, mock_urc):
        mock_session = mock.Mock()
        report_list = ['stale_dev', 'stale_draft', 'stale_qa', 'stale_km', 'conflicts', 'warnings']
        authors = {'shadowman': {'username': 'shadowman', 'name': 'Shadow Man',
                                 'email': 'shadowman@redhat.com', 'sst': 'rhel-sst-foo'}}
        mrs_to_conflict_check = self.MOCK_MR_LIST
        mock_sstdata.return_value = (['foo'], [])
        mock_last_update.return_value = "I looked at this recently, I swear"
        report_generator.examine_mrs(mock_session, report_list, authors, mrs_to_conflict_check)
        self.assertEqual(mock_urc.call_count, 2)
        mock_urc.assert_called_with(mock.ANY, mock.ANY, self.MOCK_MR2, 'rhel-sst-foo')

        authors = {'shadowman': {'username': 'shadowman', 'name': 'Shadow Man',
                                 'email': 'shadowman@redhat.com', 'sst': 'NO-RHEL-SST-FOUND'}}
        mock_urc.reset_mock()
        report_generator.examine_mrs(mock_session, report_list, authors, mrs_to_conflict_check)
        self.assertEqual(mock_urc.call_count, 2)

    @mock.patch.dict(os.environ, {'REPORTER_EMAIL_FROM': 'shadowman@redhat.com',
                                  'LDAP_SERVER': 'ldap.example.com',
                                  'REQUESTS_CA_BUNDLE': '/etc/pki/certs/whatever.ca'})
    def test_get_parser_args(self):
        with mock.patch("sys.argv", ["_get_parser_args", "-c", "-p", "cki-project/kernel-ark"]):
            args = report_generator._get_parser_args()
            self.assertEqual(args.stale, 15)
            self.assertEqual(args.draft_stale, 90)
            self.assertEqual(args.from_address, 'shadowman@redhat.com')
            self.assertEqual(args.ldap_server, 'ldap.example.com')
            self.assertEqual(args.sentry_ca_certs, '/etc/pki/certs/whatever.ca')

    @mock.patch('webhook.utils.report_generator.ReportMR', mock.Mock())
    @mock.patch('webhook.utils.report_generator.send_emailed_report', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.report_generator.format_report_body', mock.Mock(return_value='xyz'))
    @mock.patch('webhook.utils.report_generator.examine_mrs', mock.Mock(return_value=(True, True)))
    @mock.patch('webhook.utils.report_generator.get_author_info', mock.Mock(return_value={}))
    @mock.patch('webhook.utils.report_generator.section_info_builder', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.report_generator.get_open_mrs')
    def test_main_open_mrs(self, mock_mrs):
        mock_args = mock.Mock(owners_yaml='foo', email='', groups=[], projects=['foo/bar/blah'])
        mock_mrs.return_value = self.MOCK_MR_LIST
        mock_instance = mock.Mock()
        mock_instance.projects.git.return_value = mock.Mock()
        self.response_gl_auth()
        self.response_gql_user_data()
        with self.assertLogs('cki.webhook.utils.report_generator', level='INFO') as logs:
            report_generator.main(mock_args)
            self.assertIn("Finding open MRs", logs.output[-3])
            self.assertIn("Found SST Mailing lists to cc:", logs.output[-2])
            self.assertIn("Report:\nxyzAdditional info:", logs.output[-1])

"""Webhook interaction tests."""
import copy
from dataclasses import dataclass
from subprocess import CalledProcessError
from subprocess import CompletedProcess
from unittest import mock

from tests import fakes
from tests.helpers import KwfTestCase
from webhook import configshook
from webhook import defs
from webhook.base_mr_mixins import DependsMixin
from webhook.common import get_arg_parser
from webhook.description import Description as Desc
from webhook.session import SessionRunner
from webhook.session_events import create_event
from webhook.users import User

PATCH_H = ("@@ -0,0 +1,117 @@\n"
           "+.. SPDX-License-Identifier: GPL-2.0\n+\n+====================\n"
           "+Kernel Testing Guide\n+====================\n+\n+\n"
           "+There are a number of different tools for testing the Linux kernel, so knowing\n"
           "+when to use each of them can be a challenge. This document provides a rough\n"
           "+overview of their differences, and how they fit together.\n+\n+\n"
           "+Writing and Running Tests\n+=========================\n+\n"
           "+The bulk of kernel tests are written using either the kselftest or KUnit\n"
           "+frameworks. These both provide infrastructure to help make running tests and\n"
           "+groups of tests easier, as well as providing helpers to aid in writing new\n"
           "+tests.\n+\n"
           "+If you're looking to verify the behaviour of the Kernel — particularly specific\n"
           "+parts of the kernel — then you'll want to use KUnit or kselftest.\n+\n+\n"
           "+The Difference Between KUnit and kselftest\n"
           "+------------------------------------------\n+\n"
           "+KUnit (Documentation/dev-tools/kunit/index.rst) is an entirely in-kernel system\n"
           "+for \"white box\" testing: because test code is part of the kernel, it can access\n"
           "+internal structures and functions which aren't exposed to userspace.\n+\n"
           "+KUnit tests therefore are best written against small, self-contained parts\n"
           "+of the kernel, which can be tested in isolation. This aligns well with the\n"
           "+concept of 'unit' testing.\n+\n"
           "+For example, a KUnit test might test an individual kernel function (or even a\n"
           "+single codepath through a function, such as an error handling case), rather\n"
           "+than a feature as a whole.\n+\n"
           "+This also makes KUnit tests very fast to build and run, allowing them to be\n"
           "+run frequently as part of the development process.\n+\n"
           "+There is a KUnit test style guide which may give further pointers in\n"
           "+Documentation/dev-tools/kunit/style.rst\n+\n+\n"
           "+kselftest (Documentation/dev-tools/kselftest.rst), on the other hand, is\n"
           "+largely implemented in userspace, and tests are normal userspace scripts or\n"
           "+programs.\n+\n"
           "+This makes it easier to write more complicated tests, or tests which need to\n"
           "+manipulate the overall system state more (e.g., spawning processes, etc.).\n"
           "+However, it's not possible to call kernel functions directly from kselftest.\n"
           "+This means that only kernel functionality which is exposed to userspace somehow\n"
           "+(e.g. by a syscall, device, filesystem, etc.) can be tested with kselftest.  To\n"
           "+work around this, some tests include a companion kernel module which exposes\n"
           "+more information or functionality. If a test runs mostly or entirely within the\n"
           "+kernel, however,  KUnit may be the more appropriate tool.\n+\n"
           "+kselftest is therefore suited well to tests of whole features, as these will\n"
           "+expose an interface to userspace, which can be tested, but not implementation\n"
           "+details. This aligns well with 'system' or 'end-to-end' testing.\n+\n"
           "+For example, all new system calls should be accompanied by kselftest tests.\n+\n"
           "+Code Coverage Tools\n+===================\n+\n"
           "+The Linux Kernel supports two different code coverage measurement tools. These\n"
           "+can be used to verify that a test is executing particular functions or lines\n"
           "+of code. This is useful for determining how much of the kernel is being tested,\n"
           "+and for finding corner-cases which are not covered by the appropriate test.\n+\n"
           "+:doc:`gcov` is GCC's coverage testing tool, which can be used with the kernel\n"
           "+to get global or per-module coverage. Unlike KCOV, it does not record per-task\n"
           "+coverage. Coverage data can be read from debugfs, and interpreted using the\n"
           "+usual gcov tooling.\n+\n"
           "+:doc:`kcov` is a feature which can be built in to the kernel to allow\n"
           "+capturing coverage on a per-task level. It's therefore useful for fuzzing and\n"
           "+other situations where information about code executed during, for example, a\n"
           "+single syscall is useful.\n+\n+\n+Dynamic Analysis Tools\n"
           "+======================\n+\n"
           "+The kernel also supports a number of dynamic analysis tools, which attempt to\n"
           "+detect classes of issues when they occur in a running kernel. These typically\n"
           "+each look for a different class of bugs, such as invalid memory accesses,\n"
           "+concurrency issues such as data races, or other undefined behaviour like\n"
           "+integer overflows.\n+\n"
           "+Some of these tools are listed below:\n+\n"
           "+* kmemleak detects possible memory leaks. See\n"
           "+  Documentation/dev-tools/kmemleak.rst\n"
           "+* KASAN detects invalid memory accesses such as out-of-bounds and\n"
           "+  use-after-free errors. See Documentation/dev-tools/kasan.rst\n"
           "+* UBSAN detects behaviour that is undefined by the C standard, like integer\n"
           "+  overflows. See Documentation/dev-tools/ubsan.rst\n"
           "+* KCSAN detects data races. See Documentation/dev-tools/kcsan.rst\n"
           "+* KFENCE is a low-overhead detector of memory issues, which is much faster than\n"
           "+  KASAN and can be used in production. See Documentation/dev-tools/kfence.rst\n"
           "+* lockdep is a locking correctness validator. See\n"
           "+  Documentation/locking/lockdep-design.rst\n"
           "+* There are several other pieces of debug instrumentation in the kernel, many\n"
           "+  of which can be found in lib/Kconfig.debug\n+\n"
           "+These tools tend to test the kernel as a whole, and do not \"pass\" like\n"
           "+kselftest or KUnit tests. They can be combined with KUnit or kselftest by\n"
           "+running tests on a kernel with these tools enabled: you can then be sure\n"
           "+that none of these errors are occurring during the test.\n+\n"
           "+Some of these tools integrate with KUnit or kselftest and will\n"
           "+automatically fail tests if an issue is detected.\n+\n")


def create_configs_mr(mr_url) -> configshook.ConfigsMR:
    """Return a fresh ConfigsMR object."""
    session = SessionRunner.new('configshook', args='')
    gl_instance = fakes.FakeGitLab()
    gl_instance.user = User(username='cki-kwf-bot')
    session.get_gl_instance = mock.Mock(return_value=gl_instance)

    return configshook.ConfigsMR.new(session, mr_url, source_path='/somewhere/fake',
                                     linux_src='/another/fake/place')


@dataclass(repr=False)
class JsonConfigsMR:
    """Simple collection of file names of json data and users to build ConfigsMR from."""

    mr_url: defs.GitlabURL
    basemr: str
    commits: str


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml',
                                'OWNERS_YAML': 'tests/assets/owners-snapshot.yaml'})
@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestConfigsEval(KwfTestCase):
    """Test for the configshook webhook."""

    # For mocking subprocess.run
    _mocked_runs = []
    _mocked_calls = []

    PATH_WITH_NAMESPACE = 'redhat/centos-stream/src/kernel/centos-stream-9'
    PROJECT_URL = f'https://gitlab.com/{PATH_WITH_NAMESPACE}'
    MR_URL = f'{PROJECT_URL}/-/merge_requests/4776'
    PAYLOAD_MERGE = {'object_kind': 'merge_request',
                     'project': {'id': 1,
                                 'archived': False,
                                 'web_url': PROJECT_URL,
                                 'path_with_namespace': PATH_WITH_NAMESPACE
                                 },
                     'object_attributes': {'target_branch': 'main',
                                           'iid': 2,
                                           'url': MR_URL},
                     'changes': {'labels': {'previous': [],
                                            'current': []}},
                     'description': 'dummy description',
                     'state': 'opened',
                     'user': {'username': 'test_user'}
                     }

    PAYLOAD_NOTE = {'object_kind': 'note',
                    'project': {'id': 1,
                                'archived': False,
                                'web_url': PROJECT_URL,
                                'path_with_namespace': PATH_WITH_NAMESPACE
                                },
                    'object_attributes': {'note': 'comment',
                                          'noteable_type': 'MergeRequest'},
                    'merge_request': {'target_branch': 'main',
                                      'iid': 2,
                                      'url': MR_URL},
                    'description': 'dummy description',
                    'state': 'opened',
                    'user': {'username': 'test_user'}
                    }

    def _mock_run(self, args, *, check=False, **_):
        for run in self._mocked_runs:
            if run[0] == args:
                (returncode, stdout) = run[1:]
                break
        else:
            self.fail(f'Command {args} not found in mocked subprocess.run')
        self._mocked_calls.append(args)
        if returncode:
            raise CalledProcessError(returncode, args, output=stdout)
        return CompletedProcess(args, returncode, stdout=stdout)

    def _add_run_result(self, args, returncode, stdout=None):
        self._mocked_runs.append((args, returncode, stdout))

    def populate_responses(self, json_configs_mr) -> None:
        # Make sure we're starting with an empty set of responses
        self.responses.reset()

        # Standard user authentication
        self.response_gl_auth()
        # Standard GraphQL user data response
        self.response_gql_user_data()

        mr_endpoint = ''
        # Set up the ConfigsMR query responses.
        variables = {'namespace': json_configs_mr.mr_url.namespace,
                     'mr_id': str(json_configs_mr.mr_url.id)}

        basemr = self.load_yaml_asset(
            path=json_configs_mr.basemr,
            sub_module='gitlab_graphql_api'
        )
        self.add_query(configshook.ConfigsMR.MR_QUERY, variables=variables, query_result=basemr)

        commits = self.load_yaml_asset(
            path=json_configs_mr.commits,
            sub_module='gitlab_graphql_api'
        )
        self.add_query(configshook.ConfigsMR.COMMIT_QUERY,
                       variables=variables, query_result=commits)

        proj_name = json_configs_mr.mr_url.namespace.split('/')[-1]
        project = self.load_yaml_asset(
            path=f'project-{proj_name}.json',
            sub_module='gitlab_rest_api'
        )

        proj_id = basemr['data']['project']['mr']['project']['id'].split('/')[-1]
        proj_endpoint = "https://gitlab.com/api/v4/projects"
        proj_path = json_configs_mr.mr_url.namespace.replace("/", "%2F")

        self.responses.get(f'{proj_endpoint}/{proj_id}', json=project)
        self.responses.get(f'{proj_endpoint}/{proj_path}', json=project)

        merge_request = self.load_yaml_asset(
            path=f'mr-{json_configs_mr.mr_url.id}-{proj_name}.json',
            sub_module='gitlab_rest_api'
        )
        mr_endpoint = f'{proj_endpoint}/{proj_id}/merge_requests/{json_configs_mr.mr_url.id}'
        self.responses.get(mr_endpoint, json=merge_request)

    def setup_cs9_mr(self, mr_id) -> configshook.ConfigsMR:
        mr_url = defs.GitlabURL('https://gitlab.com/redhat/centos-stream/src/kernel/'
                                f'centos-stream-9/-/merge_requests/{mr_id}')
        proj_name = mr_url.namespace.split('/')[-1]

        basemr = f'mr_{proj_name}_{mr_id}-basemr.json'
        commits = f'mr_{proj_name}_{mr_id}-commits.json'
        json_configs_mr = JsonConfigsMR(mr_url=mr_url, basemr=basemr, commits=commits)
        self.populate_responses(json_configs_mr)

        return create_configs_mr(mr_url)

    @mock.patch('webhook.common.get_commits_count')
    @mock.patch('webhook.common.extract_dependencies')
    @mock.patch('webhook.cdlib.get_dependencies_data')
    @mock.patch('webhook.cdlib.get_submitted_diff')
    def test_merge_request(self, sdiff, dep_data, ext_deps, gcc):
        """Check handling of a merge request."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload['object_attributes']['target_branch'] = 'os-build'
        payload['object_attributes']['action'] = 'open'
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT", []
        dep_data.return_value = (False, 'abcd')
        ext_deps.return_value = []
        gcc.return_value = 100, 50
        self._test_payload(True, payload=payload)

    @mock.patch('webhook.common.extract_dependencies')
    @mock.patch('webhook.cdlib.get_dependencies_data')
    @mock.patch('webhook.cdlib.get_submitted_diff')
    def test_ucid_re_evaluation(self, sdiff, dep_data, ext_deps):
        """Check handling of commit ID re-evaluation."""
        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = "request-commit-id-evaluation"
        payload["object_attributes"]["noteable_type"] = "MergeRequest"
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT", []
        dep_data.return_value = (False, 'abcd')
        ext_deps.return_value = []
        self._test_payload(True, payload)
        payload["object_attributes"]["state"] = "closed"
        self._test_payload(False, payload)
        payload["project"]["archived"] = True
        self._test_payload(False, payload)

    def test_match_description(self):
        """Check that we get sane strings back for match types."""
        output = configshook.ConfigMatch.NOT_A_CONFIG.description
        self.assertEqual(output, "Not a Config")
        output = configshook.ConfigMatch.OK_CONFIG.description
        self.assertEqual(output, "OK Config")
        output = configshook.ConfigMatch.BAD_CONFIG.description
        self.assertEqual(output, "Bad Config")
        output = configshook.ConfigMatch.ARK_CONFIG_MISMATCH.description
        self.assertEqual(output, "ARK Config Mismatch")

    def test_config_eval_report(self):
        """Check on reporting functions."""
        cmr = mock.Mock()
        cmr.merge_request = mock.Mock()
        cmr.commit_count = 4
        cmr.footnotes = []
        cmr.mr_config_diffs = ""
        cmr.mergeable = False
        rhconfig1 = mock.Mock(sha='6cb6a4a3ede59f047febaeaa801f164954541ff0',
                              match=configshook.ConfigMatch.OK_CONFIG, footnotes=['1'])
        rhconfig2 = mock.Mock(sha='12345678aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
                              match=configshook.ConfigMatch.NOT_A_CONFIG, footnotes=[])
        rhconfig3 = mock.Mock(sha='1cd738b13ae9b29e03d6149f0246c61f76e81fcf',
                              match=configshook.ConfigMatch.BAD_CONFIG, footnotes=['2'])
        rhconfig4 = mock.Mock(sha='348742bd816fbbcda2f8243bf234bf1c91788082',
                              differences="Things got weird here",
                              match=configshook.ConfigMatch.ARK_CONFIG_MISMATCH, footnotes=['3'])
        cmr.rhconfigs = [rhconfig1, rhconfig2, rhconfig3, rhconfig4]
        cmr.configs_approved = False
        cmr.has_ark_config_mismatch = True
        cmr.has_bad_config = True
        cmr.mr_config_diffs = "MR had config diffs\n"
        cmr.merged_config_diffs = "Merged results had config diffs\n"
        report = configshook.ConfigsMR.config_eval_report(cmr)
        self.assertIn("348742bd816fbbcda2f8243bf234bf1c91788082|ARK Config Mismatch", report)
        self.assertIn("1cd738b13ae9b29e03d6149f0246c61f76e81fcf|Bad Config", report)
        self.assertIn("6cb6a4a3ede59f047febaeaa801f164954541ff0|OK Config", report)
        self.assertIn("MR had config diffs", report)
        self.assertIn("Merged results had config diffs", report)
        cmr.mergeable = True
        cmr.configs_approved = True
        report = configshook.ConfigsMR.config_eval_report(cmr)
        self.assertIn("Merge Request **passes** basic config evaluation.", report)

    def test_commit_notes(self):
        cmr = self.setup_cs9_mr(4776)

        rhconfig = mock.Mock(ucids=[], match=configshook.ConfigMatch.NOT_A_CONFIG)
        cmr.update_config_match_data(rhconfig, configshook.ConfigMatch.NOT_A_CONFIG)
        self.assertEqual('', configshook.ConfigMatch.NOT_A_CONFIG.footnote)
        self.assertEqual(rhconfig.match, configshook.ConfigMatch.NOT_A_CONFIG)

        cmr.update_config_match_data(rhconfig, configshook.ConfigMatch.OK_CONFIG)
        self.assertEqual(cmr.footnotes[0], configshook.ConfigMatch.OK_CONFIG.footnote)
        self.assertEqual(cmr.footnote_ids[configshook.ConfigMatch.OK_CONFIG], '1')
        self.assertEqual(rhconfig.match, configshook.ConfigMatch.OK_CONFIG)

        cmr.footnotes = []
        cmr.update_config_match_data(rhconfig, configshook.ConfigMatch.BAD_CONFIG)
        self.assertEqual(cmr.footnotes[0], configshook.ConfigMatch.BAD_CONFIG.footnote)
        self.assertEqual(cmr.footnote_ids[configshook.ConfigMatch.BAD_CONFIG], '1')
        self.assertEqual(rhconfig.match, configshook.ConfigMatch.BAD_CONFIG)

        cmr.footnotes = []
        cmr.update_config_match_data(rhconfig, configshook.ConfigMatch.ARK_CONFIG_MISMATCH)
        self.assertEqual(cmr.footnotes[0], configshook.ConfigMatch.ARK_CONFIG_MISMATCH.footnote)
        self.assertEqual(cmr.footnote_ids[configshook.ConfigMatch.ARK_CONFIG_MISMATCH], '1')
        self.assertEqual(rhconfig.match, configshook.ConfigMatch.ARK_CONFIG_MISMATCH)

    def test_labels_and_scopes(self):
        cmr = self.setup_cs9_mr(4776)

        cmr.mergeable = False
        rhconfig = mock.Mock(match=configshook.ConfigMatch.OK_CONFIG)
        cmr.rhconfigs = [rhconfig]
        self.assertFalse(cmr.has_bad_config)
        self.assertFalse(cmr.has_ark_config_mismatch)
        self.assertTrue(cmr.configs_approved)
        self.assertEqual(defs.MrScope.NEEDS_REVIEW, cmr.overall_configs_scope)
        self.assertEqual("Configs::NeedsReview", cmr.configs_label)

        cmr.errors = ""
        cmr.mergeable = True
        rhconfig = mock.Mock(match=configshook.ConfigMatch.BAD_CONFIG)
        cmr.rhconfigs = [rhconfig]
        self.assertTrue(cmr.has_bad_config)
        self.assertFalse(cmr.has_ark_config_mismatch)
        self.assertFalse(cmr.configs_approved)
        self.assertEqual(defs.MrScope.NEEDS_REVIEW, cmr.overall_configs_scope)
        self.assertEqual("Configs::NeedsReview", cmr.configs_label)

        cmr.errors = ""
        rhconfig.match = configshook.ConfigMatch.ARK_CONFIG_MISMATCH
        rhconfig.footnotes = []
        self.assertFalse(cmr.has_bad_config)
        self.assertTrue(cmr.has_ark_config_mismatch)
        self.assertFalse(cmr.configs_approved)
        self.assertEqual(defs.MrScope.OK, cmr.overall_configs_scope)
        self.assertEqual("Configs::OK", cmr.configs_label)
        self.assertTrue(cmr.has_ark_config_mismatch)

        rhconfig.match = configshook.ConfigMatch.OK_CONFIG
        self.assertFalse(cmr.has_bad_config)
        self.assertFalse(cmr.has_ark_config_mismatch)
        self.assertTrue(cmr.configs_approved)
        self.assertEqual(defs.MrScope.OK, cmr.overall_configs_scope)
        self.assertEqual("Configs::OK", cmr.configs_label)
        self.assertFalse(cmr.has_ark_config_mismatch)

        expected_labels = ["Configs::OK", "Configuration"]
        self.assertCountEqual(expected_labels, cmr.expected_labels)

    def _test_payload(self, result, payload):
        with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'production'}):
            self.clear_caches()
            self._test(result, payload)
        with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'development'}):
            self.clear_caches()
            self._test(result, payload)

    @mock.patch.object(configshook.ConfigsMR, 'assemble_configs', mock.Mock())
    @mock.patch.object(DependsMixin, '_set_mr_blocks', mock.Mock())
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment')
    @mock.patch('webhook.configshook.ConfigsMR.remove_labels')
    @mock.patch('webhook.configshook.ConfigsMR.add_labels')
    def _test(self, result, payload, add_labels, remove_labels, update_webhook_comment):
        # setup dummy gitlab data
        target = "main"
        if payload['object_kind'] == 'merge_request':
            target = payload['object_attributes']['target_branch']
        if payload['object_kind'] == 'note':
            target = payload['merge_request']['target_branch']
        merge_request = mock.Mock(target_branch=target)
        merge_request.iid = 4776

        # We're not actually using the ConfigsMR object set up, just the responses
        self.setup_cs9_mr(merge_request.iid)

        parser = get_arg_parser('configshook')
        parser.add_argument('--rhkernel-src')
        parser.add_argument('--linux-src')
        arg_str = '--rhkernel-src /src/kernel-ark --linux-src /src/kernel-ark'
        args = parser.parse_args(arg_str.split())
        args.disable_user_check = True

        mock_session = SessionRunner.new('configshook', args, configshook.HANDLERS)
        mock_session.rh_projects = mock.Mock()
        event = create_event(mock_session, {'message-type': 'gitlab'}, payload)
        event.gl_mr = merge_request

        with mock.patch.object(configshook.ConfigsMR, 'project_remote', 'remote_name'):
            configshook.process_gl_event({}, mock_session, event)

        if result:
            add_labels.assert_not_called()
            remove_labels.assert_not_called()
            update_webhook_comment.assert_called()

    @mock.patch('pathlib.Path.read_text')
    def test_read_config_content(self, mock_read):
        config_worktree = '/tmp/worktree'
        filename = 'redhat/configs/common/generic/CONFIG_RV'
        arkfilename = 'redhat/configs/rhel/generic/CONFIG_RV'

        with mock.patch('pathlib.Path.exists', return_value=True):
            configshook.fetch_file_content(config_worktree, filename, arkfilename)
            mock_read.assert_called_with(encoding='ascii')

    def test_read_config_content_that_no_exists(self):
        config_worktree = '/foo/bar/baz'
        filename = 'redhat/configs/common/generic/CONFIG_RV'
        arkfilename = 'redhat/configs/rhel/generic/CONFIG_RV'

        self.assertEqual(configshook.fetch_file_content(config_worktree, filename, arkfilename),
                         (None, None))

    def test_has_ark_config_diffs(self):
        rhconfig = mock.Mock()
        rhconfig.sha = "abcd12345678"
        rhconfig.differences = ""
        new_cfg = ""
        common_cfg = ""
        ark_cfg = ""

        ret = configshook.RHConfigCommit.has_ark_config_diffs(rhconfig, new_cfg,
                                                              common_cfg, ark_cfg)
        self.assertEqual("", rhconfig.differences)
        self.assertFalse(ret)

        new_cfg = "CONFIG_FOO=m"
        ret = configshook.RHConfigCommit.has_ark_config_diffs(rhconfig, new_cfg,
                                                              common_cfg, ark_cfg)
        self.assertIn("RHEL commit abcd12345678 (`CONFIG_FOO=m`) does not match",
                      rhconfig.differences)
        self.assertTrue(ret)

        common_cfg = "CONFIG_FOO=m"
        rhconfig.differences = ""
        ret = configshook.RHConfigCommit.has_ark_config_diffs(rhconfig, new_cfg,
                                                              common_cfg, ark_cfg)
        self.assertEqual("", rhconfig.differences)
        self.assertFalse(ret)

        common_cfg = ""
        ark_cfg = "CONFIG_FOO=m"
        ret = configshook.RHConfigCommit.has_ark_config_diffs(rhconfig, new_cfg,
                                                              common_cfg, ark_cfg)
        self.assertEqual("", rhconfig.differences)
        self.assertFalse(ret)

        ark_cfg = "# CONFIG_FOO is not set"
        ret = configshook.RHConfigCommit.has_ark_config_diffs(rhconfig, new_cfg,
                                                              common_cfg, ark_cfg)
        self.assertIn("RHEL commit abcd12345678 (`CONFIG_FOO=m`) does not match",
                      rhconfig.differences)
        self.assertTrue(ret)

    def test_get_config_change(self):
        path = {'oldPath': 'not/redhat/configs/CONFIG_FOO',
                'newPath': '/dev/null',
                'newFile': 'false',
                'renamedFile': 'false',
                'deletedFile': 'true',
                'aMode': '100644',
                'bMode': '100644',
                'diff': "@@ -0,0 +1 @@\n"
                        "+CONFIG_FOO=m\n"}
        setting, file = configshook.get_config_change(path)
        self.assertEqual('', setting)
        self.assertIsNone(file)

        path['oldPath'] = 'redhat/configs/CONFIG_FOO'
        setting, file = configshook.get_config_change(path)
        self.assertIsNone(setting)
        self.assertEqual('redhat/configs/CONFIG_FOO', file)

        path['deletedFile'] = 'false'
        path['renamedFile'] = 'true'
        setting, file = configshook.get_config_change(path)
        self.assertEqual('', setting)
        self.assertIsNone(file)

        path['newFile'] = 'true'
        path['renamedFile'] = 'false'
        path['oldPath'] = '/dev/null'
        path['newPath'] = 'redhat/configs/CONFIG_FOO'
        setting, file = configshook.get_config_change(path)
        self.assertEqual("CONFIG_FOO=m", setting)
        self.assertEqual('redhat/configs/CONFIG_FOO', file)

        path['newPath'] = 'not/redhat/configs/CONFIG_FOO'
        setting, file = configshook.get_config_change(path)
        self.assertEqual('', setting)
        self.assertIsNone(file)

    @mock.patch('webhook.kgit.branch_delete')
    @mock.patch('webhook.kgit.worktree_remove')
    def test_clean_up_temp_ark_branch(self, mock_remove, mock_delete):
        cmr = mock.Mock()
        cmr.source_path = '/src/linux'
        cmr.config_worktree = '/src/kernel-ark-temp-merge-branch/'
        cmr.ark_branch = 'temp-merge-branch'
        configshook.ConfigsMR.clean_up_temp_ark_branch(cmr)
        mock_remove.assert_called_with(cmr.source_path, cmr.config_worktree)
        mock_delete.assert_called_with(cmr.source_path, cmr.ark_branch)

    @mock.patch('os.path.exists', mock.Mock(return_value=False))
    @mock.patch('webhook.configshook.ConfigsMR.clean_up_temp_ark_branch')
    def test_handle_stale_worktree(self, clean_up):
        cmr = mock.Mock()
        cmr.source_path = '/src/kernel'
        cmr.ark_branch = 'kernel-ark/os-build'
        cmr.clean_up_temp_ark_branch = clean_up

        m_args = ['git', 'branch', '-D', f'{cmr.ark_branch}-save']
        self._add_run_result(m_args, 4, 'Uhhh yeah it exploded')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            configshook.ConfigsMR.handle_stale_worktree(cmr)

        with mock.patch('webhook.kgit.branch_delete') as kgbd:
            configshook.ConfigsMR.handle_stale_worktree(cmr)
            kgbd.assert_called_with(cmr.source_path, f"{cmr.ark_branch}-save")

    @mock.patch('webhook.configshook.ConfigsMR.handle_stale_worktree', mock.Mock())
    @mock.patch('os.path.isdir', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.worktree_add')
    @mock.patch('webhook.kgit.fetch_remote')
    def test_prep_temp_ark_branch(self, mock_fetch, mock_worktree_add):
        cmr = mock.Mock()
        cmr.ark_branch = ''
        cmr.config_worktree = ''
        cmr.source_path = '/path/to/rhel/'

        # ark_branch is already set up
        cmr.ark_branch = 'kernel-ark-config-check'
        configshook.ConfigsMR.prep_temp_ark_branch(cmr)
        mock_fetch.assert_not_called()
        self.assertEqual('', cmr.config_worktree)

        # needs to be set up
        cmr.ark_branch = ''
        configshook.ConfigsMR.prep_temp_ark_branch(cmr)
        mock_fetch.assert_called_once()
        self.assertEqual('/path/to/kernel-ark-config-check/', cmr.config_worktree)
        mock_worktree_add.assert_called_once()

    @mock.patch('shutil.move')
    @mock.patch('glob.glob')
    def test_move_config_files(self, mock_glob, mock_move):
        cmr = mock.Mock()
        cmr.worktree_dir = '/path/to/ark-worktree'
        cmr.config_evaluation = 'config_evaluation'
        mock_glob.return_value = [f'{cmr.worktree_dir}/redhat/configs/kernel-5.14.0-x86_64.config']
        configshook.ConfigsMR.move_config_files(cmr)
        mock_move.assert_called_with(
            '/path/to/ark-worktree/redhat/configs/kernel-5.14.0-x86_64.config',
            '/path/to/ark-worktree/config_evaluation/kernel-5.14.0-x86_64.config'
        )

    @mock.patch('webhook.configshook.make_dist_configs', mock.Mock())
    @mock.patch('subprocess.run', mock.Mock())
    @mock.patch('webhook.configshook.ConfigsMR.move_config_files', mock.Mock())
    @mock.patch('webhook.kgit.merge', mock.Mock())
    @mock.patch('webhook.kgit.commit', mock.Mock())
    @mock.patch('webhook.kgit.add', mock.Mock())
    @mock.patch('os.mkdir', mock.Mock())
    @mock.patch('webhook.kgit.raw_diff')
    def test_assemble_configs(self, mock_raw_diff):
        cmr = mock.Mock()
        cmr.worktree_dir = '/path/to/ark-worktree'
        cmr.config_evaluation = ''
        cmr.project_remote = 'centos-stream-9'
        cmr.iid = '66'
        cmr.target_branch = 'main'
        cmr.mr_config_diffs = ''
        cmr.merged_config_diffs = ''
        raw_diff_output = mock.Mock()
        raw_diff_output.stdout = 'This was a diff'
        mock_raw_diff.return_value = raw_diff_output
        configshook.ConfigsMR.assemble_configs(cmr)
        self.assertIn('This was a diff', cmr.merged_config_diffs)
        raw_diff_output.stdout = ''
        configshook.ConfigsMR.assemble_configs(cmr)
        self.assertIn('config changes had ZERO impact', cmr.merged_config_diffs)

    @mock.patch('webhook.configshook.make_dist_configs', mock.Mock())
    @mock.patch('webhook.kgit.commit', mock.Mock())
    @mock.patch('webhook.kgit.add', mock.Mock())
    @mock.patch('os.mkdir', mock.Mock())
    def test_assemble_configs_unmergeable_mr(self):
        cmr = mock.Mock()
        cmr.worktree_dir = '/path/to/ark-worktree'
        cmr.config_evaluation = ''
        cmr.project_remote = 'centos-stream-9'
        cmr.iid = '66'
        cmr.mergeable = True
        self._mocked_calls = []
        m_args = ['make', 'dist-configs']
        self._add_run_result(m_args, 0)
        m_args_bad = ['git', 'merge', '--quiet', '--no-edit', 'centos-stream-9/merge-requests/66']
        self._add_run_result(m_args_bad, 5, "Oh no")
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            configshook.ConfigsMR.assemble_configs(cmr)
            self.assertFalse(cmr.mergeable)

    def _test_evaluate_merged_configs(self, diffs, merged, mergeable=True):
        """Check if a config matches the merged configs."""
        cmr = self.setup_cs9_mr(4776)

        c = mock.Mock(sha="abcdef012345", author_email="jdoe@redhat.com", description=Desc(""))
        c.diff = diffs

        cmr.rhconfigs = []
        rhconfig = configshook.RHConfigCommit.from_commit(c)
        cmr.rhconfigs.append(rhconfig)
        cmr.merged_config_diffs = merged
        cmr.mergeable = mergeable

        return cmr.evaluate_merged_configs(rhconfig)

    @mock.patch('subprocess.run', mock.Mock())
    @mock.patch('webhook.configshook.ConfigsMR.prep_temp_ark_branch', mock.Mock())
    def _test_evaluate_config(self, mocked_read, diffs):
        """Check if a config matches with upstream repo."""
        cmr = self.setup_cs9_mr(4776)

        c = mock.Mock(sha="abcdef012345", author_email="jdoe@redhat.com", description=Desc(""))
        c.diff = diffs

        cmr.rhconfigs = []
        rhconfig = configshook.RHConfigCommit.from_commit(c)
        cmr.rhconfigs.append(rhconfig)

        cmr.evaluate_config(rhconfig)
        return bool(rhconfig.differences)

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.configshook.read_config_content')
    def test_evaluate_config_addition_match(self, read, add_label):
        """Check if a config matches with rhel configs in upstream repo."""
        read.return_value = "CONFIG_RV=y"
        diffs = [{'oldPath': '/dev/null',
                  'newPath': 'redhat/configs/rhel/generic/CONFIG_RV',
                  'newFile': 'true',
                  'renamedFile': 'false',
                  'deletedFile': 'false',
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': "@@ -0,0 +1 @@\n"
                          "+CONFIG_RV=y\n"}]

        merged = "+CONFIG_RV=y"

        self.assertFalse(self._test_evaluate_config(read, diffs))
        self.assertFalse(self._test_evaluate_merged_configs(diffs, merged))
        self.assertTrue(self._test_evaluate_merged_configs(diffs, merged, mergeable=False))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.configshook.read_config_content')
    def test_evaluate_config_addition_match_in_ark(self, read, add_label):
        """Check if a config matches with ark configs in upstream repo."""
        read.side_effect = [None, "CONFIG_RV=y"]
        diffs = [{'oldPath': '/dev/null',
                  'newPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'newFile': 'true',
                  'renamedFile': 'false',
                  'deletedFile': 'false',
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': "@@ -0,0 +1 @@\n"
                          "+CONFIG_RV=y\n"}]

        self.assertFalse(self._test_evaluate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.configshook.read_config_content')
    def test_evaluate_config_change_match(self, read, add_label):
        """Check if a config matches with upstream repo."""
        read.return_value = "CONFIG_MEMTEST=y"
        diffs = [{'oldPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'newPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'newFile': 'false',
                  'renamedFile': 'false',
                  'deletedFile': 'false',
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': "@@ -1 +1 @@\n"
                          "-# CONFIG_MEMTEST is not set\n"
                          "+CONFIG_MEMTEST=y\n"}]

        self.assertFalse(self._test_evaluate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.configshook.read_config_content')
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment',
                mock.Mock(return_value=None))
    def test_evaluate_config_change_mismatch(self, read, add_label):
        """Check if a config matches with upstream repo."""
        read.return_value = "# CONFIG_RV is not set"
        diffs = [{'oldPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'newPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'newFile': 'false',
                  'renamedFile': 'false',
                  'deletedFile': 'false',
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': "@@ -1 +1 @@\n"
                          "-# CONFIG_MEMTEST is not set\n"
                          "+CONFIG_MEMTEST=y\n"}]

        merged = "# CONFIG_RV is not set"

        with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'}):
            self.assertTrue(self._test_evaluate_config(read, diffs))
        self.assertFalse(self._test_evaluate_merged_configs(diffs, merged))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.configshook.read_config_content')
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment',
                mock.Mock(return_value=None))
    def test_evaluate_config_addition_that_does_not_exist_in_both(self, read, add_label):
        """Check if a config file exists but it does not match with commit."""
        read.side_effect = [None, None, None, None]
        diffs = [{'oldPath': '/dev/null',
                  'newPath': 'redhat/configs/rhel/generic/CONFIG_RV',
                  'newFile': 'true',
                  'renamedFile': 'false',
                  'deletedFile': 'false',
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': "@@ -0,0 +1 @@\n"
                          "+CONFIG_RV=y\n"},
                 {"newPath": "kernel/rh_messages.c",
                  "oldPath": "kernel/rh_messages.c",
                  "aMode": "100644",
                  "bMode": "100644",
                  'newFile': 'false',
                  'renamedFile': 'false',
                  'deletedFile': 'false',
                  "diff": "@@ -163,7 +163,7 @@ void mark_tech_preview(const char *msg, struct "
                          "module *mod)\n \tif (msg)\n \t\tstr = msg;\n #ifdef CONFIG_MODULES\n"
                          "-\telse if (mod && mod->name)\n+\telse if (mod)\n "
                          "\t\tstr = mod->name;\n #endif\n \n"}]

        exp = "A CONFIG change in RHEL commit abcdef012345 (`CONFIG_RV=y`) does not match ARK"
        with self.assertLogs('cki.webhook.configshook', level='DEBUG') as logs:
            with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'}):
                self.assertTrue(self._test_evaluate_config(read, diffs))
            self.assertIn(exp, "\n".join(logs.output))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.configshook.read_config_content')
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment',
                mock.Mock(return_value=None))
    def test_evaluate_config_addition_that_exists_but_not_enabled(self, read, add_label):
        """Check if a config file exists but it does not match with commit."""
        read.return_value = "# CONFIG_RV is not set"
        diffs = [{'oldPath': '/dev/null',
                  'newPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'newFile': 'true',
                  'renamedFile': 'false',
                  'deletedFile': 'false',
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': "@@ -0,0 +1 @@\n"
                          "+CONFIG_RV=y\n"}]

        exp = "A CONFIG change in RHEL commit abcdef012345 (`CONFIG_RV=y`) does not match ARK"
        with self.assertLogs('cki.webhook.configshook', level='DEBUG') as logs:
            # Skip staging test here just to cover a different branch
            self.assertTrue(self._test_evaluate_config(read, diffs))
            self.assertIn(exp, "\n".join(logs.output))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.configshook.read_config_content')
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment',
                mock.Mock(return_value=None))
    def test_evaluate_config_addition_does_not_exist(self, read, add_label):
        """Check if a config file exists but it does not match with commit."""
        diffs = [{'oldPath': '/dev/null',
                  'newPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'newFile': 'true',
                  'renamedFile': 'false',
                  'deletedFile': 'false',
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': "@@ -0,0 +1 @@\n"
                          "+CONFIG_RV=y\n"}]

        exp = "A CONFIG change in RHEL commit abcdef012345 (`CONFIG_RV=y`) does not match ARK"
        with self.assertLogs('cki.webhook.configshook', level='DEBUG') as logs:
            with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'}):
                self.assertTrue(self._test_evaluate_config(read, diffs))
            self.assertIn(exp, "\n".join(logs.output))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.configshook.read_config_content')
    def test_evaluate_config_deletion_match(self, read, add_label):
        """Check if a config deletion matches with upstream repo."""
        read.return_value = None
        diffs = [{'newPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'oldPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'newFile': 'false',
                  'renamedFile': 'false',
                  'deletedFile': 'true',
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': "@@ -1 +0,0 @@\n"
                          "-CONFIG_RV=y\n"}]

        self.assertFalse(self._test_evaluate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.configshook.read_config_content')
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment',
                mock.Mock(return_value=None))
    def test_evaluate_config_deletion_mismatch(self, read, add_label):
        """Check if a config deletion mismatches with upstream repo."""
        read.return_value = 'CONFIG_RV=y'
        diffs = [{'newPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'oldPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'newFile': 'false',
                  'renamedFile': 'false',
                  'deletedFile': 'true',
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': "@@ -1 +0,0 @@\n"
                          "-CONFIG_RV=y\n"}]

        with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'}):
            self.assertTrue(self._test_evaluate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.configshook.read_config_content')
    def test_evaluate_config_rename_match(self, read, add_label):
        """Check if a config renaming matches with upstream repo."""
        read.return_value = None
        diffs = [{'newPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'oldPath': 'redhat/configs/rhel/generic/CONFIG_RV',
                  'newFile': 'false',
                  'renamedFile': 'true',
                  'deletedFile': 'false',
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': ""}]

        self.assertFalse(self._test_evaluate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.configshook.read_config_content')
    def test_validate_documentation_skip(self, read, add_label):
        """Check if a config renaming matches with upstream repo."""
        read.return_value = None
        diffs = [{'newPath': 'Documentation/dev-tools/testing-overview.rst',
                  'oldPath': 'Documentation/dev-tools/testing-overview.rst',
                  'newFile': 'true',
                  'renamedFile': 'false',
                  'deletedFile': 'false',
                  'generated_file': None,
                  'aMode': '0',
                  'bMode': '100644',
                  'diff': PATCH_H}]

        self.assertFalse(self._test_evaluate_config(read, diffs))

    def test_validate_x86_64_file_map(self):
        """Check if a config inside a x86_64 directory in ARK match with downstream versions."""
        filename = "redhat/configs/rhel/generic/x86/x86_64/CONFIG_NVRAM"
        commonfilename_exp = "redhat/configs/common/generic/x86/CONFIG_NVRAM"
        arkfilename_exp = "redhat/configs/rhel/generic/x86/CONFIG_NVRAM"

        commonfilename, arkfilename = configshook.x86_filename_replacement(filename)
        self.assertEqual(commonfilename, commonfilename_exp)
        self.assertEqual(arkfilename, arkfilename_exp)

        filename = "redhat/configs/common/generic/x86/x86_64/CONFIG_GENERIC_CPU"
        commonfilename_exp = "redhat/configs/common/generic/x86/CONFIG_GENERIC_CPU"
        arkfilename_exp = "redhat/configs/rhel/generic/x86/CONFIG_GENERIC_CPU"

        commonfilename, arkfilename = configshook.x86_filename_replacement(filename)
        self.assertEqual(commonfilename, commonfilename_exp)
        self.assertEqual(arkfilename, arkfilename_exp)

        filename = "redhat/configs/common/generic/CONFIG_RV"
        rhelfilename = filename.replace('common', 'rhel')
        commonfilename, arkfilename = configshook.x86_filename_replacement(filename)
        # We don't expect any change
        self.assertEqual(commonfilename, filename)
        self.assertEqual(arkfilename, rhelfilename)


class TestHelpers(KwfTestCase):
    """Tests for various helper functions."""

    def test_make_dist_configs_failed(self) -> None:
        """Returns None when the return code is not 0."""
        with mock.patch('subprocess.run') as mock_run:
            mock_run.return_value.returncode = -2
            self.assertIsNone(configshook.make_dist_configs('cwd'))

    def test_make_dist_configs_success(self) -> None:
        """Returns CompletedProcess.stdout when the return code is 0."""
        with mock.patch('subprocess.run', mock.Mock()) as mock_run:
            mock_run.return_value.returncode = 0
            self.assertEqual(configshook.make_dist_configs('cwd'), mock_run.return_value.stdout)

"""Tests for check_for_fixes."""
import os
from unittest import mock

from tests.helpers import KwfTestCase
from webhook.utils import check_for_fixes


class TestFixesChecker(KwfTestCase):
    """Tests for the various helper functions."""

    MOCK_MR = {'webUrl': 'https://gitlab.com/foo/bar/blah/-/merge_requests/66'}
    MOCK_MR2 = {'webUrl': 'https://gitlab.com/foo/bar/blah/-/merge_requests/67'}
    MOCK_MR_LIST = {'foo/bar/blah': [MOCK_MR, MOCK_MR2]}

    GQL_MRS = {'project':
               {'id': 'gid://gitlab/Project/1234',
                'mergeRequests':
                {'pageInfo': {'hasNextPage': False, 'endCursor': 'eyJjc'},
                 'nodes': [MOCK_MR, MOCK_MR2]}}}

    @mock.patch('webhook.fixes.FixesMR.new_from_query')
    def test_get_open_mrs(self, mock_new_fixes_mrs):
        mock_session = mock.Mock()
        namespace = 'foo/bar/blah'
        mock_mmr1 = mock.Mock()
        mock_mmr1.iid = 67
        mock_mmr1.is_build_mr = False
        mock_mmr2 = mock.Mock()
        mock_mmr2.iid = 68
        mock_mmr2.is_build_mr = False
        mock_mmr3 = mock.Mock()
        mock_mmr3.iid = 69
        mock_mmr3.is_build_mr = True
        mock_mmr3.title = "This is a build MR"
        mock_new_fixes_mrs.return_value = [mock_mmr1, mock_mmr2, mock_mmr3]
        results = check_for_fixes.get_open_mrs(mock_session, namespace)
        mock_new_fixes_mrs.assert_called_once()
        self.assertEqual({'foo/bar/blah': [mock_mmr1, mock_mmr2]}, results)

    @mock.patch.dict(os.environ, {'GL_PROJECTS': 'cki-project/kernel-ark',
                                  'REQUESTS_CA_BUNDLE': '/etc/pki/certs/whatever.ca'})
    def test_get_parser_args(self):
        with mock.patch("sys.argv", ["_get_parser_args", "-T"]):
            args = check_for_fixes._get_parser_args()
            self.assertTrue(args.testing)
            self.assertEqual(args.projects, ['cki-project/kernel-ark'])
            self.assertEqual(args.sentry_ca_certs, '/etc/pki/certs/whatever.ca')

    def test_main_no_src(self):
        mock_args = mock.Mock(linux_src=None)
        with self.assertLogs('cki.webhook.utils.check_for_fixes', level='DEBUG') as logs:
            check_for_fixes.main(mock.Mock(), mock_args)
            self.assertIn("No valid Linux source git found, aborting!", logs.output[-1])

    @mock.patch('webhook.kgit.fetch_all', mock.Mock(return_value=True))
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.check_for_fixes.get_open_mrs')
    def test_main_no_open_mrs(self, mock_mrs):
        mock_args = mock.Mock(linux_src='/src/linux',
                              projects=['cki-project/kernel-ark'])
        mock_mrs.return_value = {}
        with self.assertLogs('cki.webhook.utils.check_for_fixes', level='INFO') as logs:
            check_for_fixes.main(mock.Mock(), mock_args)
            self.assertIn("Fetching latest upstream git commits...", logs.output[-3])
            self.assertIn("Finding open MRs in: ['cki-project/kernel-ark']...", logs.output[-2])
            self.assertIn("No open MRs to process.", logs.output[-1])

    @mock.patch('webhook.utils.check_for_fixes.FixesMR', mock.Mock())
    @mock.patch('webhook.kgit.fetch_all', mock.Mock(return_value=True))
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.check_for_fixes.get_instance')
    @mock.patch('webhook.utils.check_for_fixes.get_open_mrs')
    def test_main_open_mrs(self, mock_mrs, mock_get_instance):
        mock_args = mock.Mock(linux_src='/src/linux',
                              groups=[],
                              projects=['foo/bar/blah'])
        mock_mr = mock.Mock()
        mock_mrs.return_value = {'foo/bar/blah': [mock_mr]}
        mock_instance = mock.Mock()
        mock_instance.projects.git.return_value = mock.Mock()
        mock_get_instance.return_value = mock_instance
        with self.assertLogs('cki.webhook.utils.check_for_fixes', level='INFO') as logs:
            check_for_fixes.main(mock.Mock(), mock_args)
            self.assertIn("Fetching latest upstream git commits...", logs.output[-2])
            self.assertIn("Finding open MRs in: ['foo/bar/blah']...",
                          logs.output[-1])

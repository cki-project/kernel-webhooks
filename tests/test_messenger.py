"""Tests for the webhook.messenger module."""
import os
from unittest import mock

from cki_lib.yaml import ValidationError

from tests.helpers import KwfTestCase
from webhook import messenger
from webhook.defs import UMB_BRIDGE_MESSAGE_TYPE
from webhook.session import SessionRunner
from webhook.session_events import UmbBridgeEvent
from webhook.session_events import create_event
from webhook.temp_utils import with_yaml_validation


class TestMessenger(KwfTestCase):
    """Tests for the webhook.messenger module."""

    def test_create_exception_bad_input(self) -> None:
        """Raises an exception when neither a gitlab_url nor a jira_key is given."""
        with self.assertRaises(RuntimeError):
            messenger.KwfMessage.create(
                queue_name='queue',
                exchange='exchange_name',
                target_hook='jirahook',
                source_hook='umb_bridge',
            )

    def test_create_gitlab_url_message(self) -> None:
        """Creates a new KwfMessage with a gitlab_url."""
        test_message = messenger.KwfMessage.create(
            queue_name='myqueue',
            exchange='myexchange',
            target_hook='jirahook',
            source_hook='umb_bridge',
            source_id='myuuid',
            gitlab_url='https://gitlab.com/group/project/-/merge_requests/1'
        )

        # Routing params
        self.assertEqual(test_message.queue_name, 'myqueue')
        self.assertEqual(test_message.exchange, 'myexchange')

        # Header values
        self.assertEqual(test_message.headers.event_target_webhook, 'jirahook')
        self.assertEqual(test_message.headers.event_source_webhook, 'umb_bridge')
        self.assertEqual(test_message.headers.event_source_id, 'myuuid')
        self.assertEqual(test_message.headers.event_source_environment, 'development')
        self.assertEqual(test_message.headers.message_type, UMB_BRIDGE_MESSAGE_TYPE)

        # Data (body) values
        self.assertEqual(
            test_message.data.gitlab_url,
            'https://gitlab.com/group/project/-/merge_requests/1'
        )
        self.assertEqual(test_message.data.jira_key, '')

    def test_create_jira_key_message(self) -> None:
        """Creates a new KwfMessage with a jira_key."""
        test_message = messenger.KwfMessage.create(
            queue_name='myqueue',
            exchange='myexchange',
            target_hook='backporter',
            source_hook='umb_bridge',
            source_id='',
            jira_key='RHEL-123'
        )

        # Routing params
        self.assertEqual(test_message.queue_name, 'myqueue')
        self.assertEqual(test_message.exchange, 'myexchange')

        # Header values
        self.assertEqual(test_message.headers.event_target_webhook, 'backporter')
        self.assertEqual(test_message.headers.event_source_webhook, 'umb_bridge')
        self.assertEqual(test_message.headers.event_source_id, '')
        self.assertEqual(test_message.headers.event_source_environment, 'development')
        self.assertEqual(test_message.headers.message_type, UMB_BRIDGE_MESSAGE_TYPE)

        # Data (body) values
        self.assertEqual(test_message.data.gitlab_url, '')
        self.assertEqual(test_message.data.jira_key, 'RHEL-123')

    @with_yaml_validation
    def test_create_exception_schema_validation(self) -> None:
        """Raises an exception due to values that do not match the schema."""
        with self.assertRaises(ValidationError):
            messenger.KwfMessage.create(
                queue_name='myqueue',
                exchange='myexchange',
                target_hook='backporter',
                source_hook='umb_bridge',
                jira_key=123  # per schema, int is not an acceptable value for jira_kay
            )

    def test_as_dict_output(self) -> None:
        """Generates a dict with the expected values."""
        # The 'message_type' attribute name should be translated to 'message-type' in the dict.
        expected_dict = {
            'queue_name': 'queue_name',
            'exchange': 'exchange_name',
            'headers': {
                'event_source_environment': 'development',
                'event_source_id': 'abc123',
                'event_source_webhook': 'umb_bridge',
                'event_target_webhook': 'jirahook',
                'message-type': 'cki.kwf.umb-bz-event'
            },
            'data': {
                'gitlab_url': 'https://gitlab.com/group/project/-/merge_requests/123',
                'jira_key': ''
            },
        }
        test_message = messenger.KwfMessage.create(
            queue_name='queue_name',
            exchange='exchange_name',
            target_hook='jirahook',
            source_hook='umb_bridge',
            source_id='abc123',
            gitlab_url='https://gitlab.com/group/project/-/merge_requests/123'
        )
        self.assertDictEqual(expected_dict, test_message.asdict())

    def test_sets_environment(self) -> None:
        """Sets 'event_source_environment' to the expected value."""
        message_params = {
            'queue_name': 'queue_name',
            'exchange': 'exchange_name',
            'target_hook': 'backporter',
            'source_hook': 'umb_bridge',
            'jira_key': 'RHEL-999999'
        }
        for env_name in ('development', 'staging', 'production'):
            env_dict = {'CKI_DEPLOYMENT_ENVIRONMENT': env_name}
            with self.subTest(environment=env_dict):
                with mock.patch.dict(os.environ, env_dict, clear=True):
                    test_message = messenger.KwfMessage.create(**message_params)
                    self.assertEqual(test_message.headers.event_source_environment, env_name)


class TestUmbBridgeEventCompat(KwfTestCase):
    """Tests to ensure a UmbBridgeEvent works with KwfMessage headers/body."""

    def test_umb_bridge_event(self) -> None:
        """Loads the headers and data (body) from a KwfMessage without error."""
        jira_key_message = messenger.KwfMessage.create(
            queue_name='myqueue',
            exchange='myexchange',
            target_hook='backporter',
            source_hook='umb_bridge',
            source_id='',
            jira_key='RHEL-123'
        )
        gitlab_url_message = messenger.KwfMessage.create(
            queue_name='aqueue',
            exchange='aexchange',
            target_hook='jirahook',
            source_hook='umb_bridge',
            source_id='myuuid',
            gitlab_url='https://gitlab.com/group/project/-/merge_requests/555'
        )
        for test_message in [jira_key_message, gitlab_url_message]:
            event_gl_url = test_message.data.gitlab_url if test_message.data.gitlab_url else None

            event_mr_url = test_message.data.gitlab_url if \
                '/-/merge_requests/' in test_message.data.gitlab_url else None

            event_jira_key = test_message.data.jira_key if test_message.data.jira_key else None

            with self.subTest(message=test_message):
                test_session = SessionRunner.new('jirahook', 'args')
                message_dict = test_message.asdict()
                test_event = create_event(
                    test_session,
                    message_dict['headers'],
                    message_dict['data']
                )

                self.assertIsInstance(test_event, UmbBridgeEvent)

                self.assertEqual(test_message.headers.message_type, test_event.type.value)
                self.assertEqual(event_gl_url, test_event.gitlab_url)
                self.assertEqual(event_mr_url, test_event.mr_url)
                self.assertEqual(event_jira_key, test_event.jira_key)

#!/usr/bin/env python
"""Look for upstream Fixes: commits that we need to consider backporting."""

import argparse
import os
import subprocess

from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import get_instance
import sentry_sdk

from webhook import defs
from webhook import kgit
from webhook.base_mr import BASE_MR_FIELDS
from webhook.fixes import FixesMR
from webhook.session import SessionRunner

LOGGER = logger.get_logger('webhook.utils.check_for_fixes')

# Get all open MRs.
MR_QUERY = """
query mrData($namespace: ID!, $first: Boolean = true, $skip_files: Boolean = true, $after: String = "") {
  project(fullPath: $namespace) {
    id @include(if: $first)
    mergeRequests(state: opened, after: $after,
                  not: {labels: ["NoCommits", "Oversize", "InactiveBranch", "ZStreamBuild"]}) {
      pageInfo {hasNextPage endCursor}
      nodes {webUrl ...BaseMR}
    }
  }
}
"""  # noqa: E501

MR_QUERY += BASE_MR_FIELDS


def get_open_mrs(session, namespace):
    """Return a list of the MR objects we're interested in."""
    fixes_mrs = FixesMR.new_from_query(session, query_string=MR_QUERY,
                                       query_params={'namespace': namespace},
                                       paged_key='project/mergeRequests',
                                       mrs_key='project/mergeRequests/nodes',
                                       linux_src=session.args.linux_src,
                                       kerneloscope_server_url=session.args.kerneloscope_server_url)
    filtered_mrs = {}
    for fixes_mr in fixes_mrs:
        if fixes_mr.is_build_mr:
            LOGGER.info("Skipping maintainer build MR: %s", fixes_mr.title)
            continue
        if namespace in filtered_mrs:
            filtered_mrs[namespace].append(fixes_mr)
            continue
        filtered_mrs[namespace] = [fixes_mr]

    LOGGER.debug('Project %s MRs: found %s', namespace, filtered_mrs)
    return filtered_mrs


def _get_parser_args():
    parser = argparse.ArgumentParser(description='Check for upstream Fixes against open MRs')

    # Global options
    parser.add_argument('-p', '--projects', default=os.environ.get('GL_PROJECTS', '').split(),
                        help='gitlab projects to fetch MRs from.', nargs='+', required=False)
    parser.add_argument('-l', '--linux-src', default=os.environ.get('LINUX_SRC', ''),
                        help='Directory containing upstream Linux kernel source git tree')
    parser.add_argument('-k', '--kerneloscope-server-url',
                        default=os.environ.get('KERNELOSCOPE_SERVER_URL', ''),
                        help='Kerneloscope server URL')
    parser.add_argument('--no-kerneloscope', action='store_true',
                        default=os.environ.get('NO_KERNELOSCOPE', ''),
                        help='Override use of kerneloscope, fall back to git log methods')
    parser.add_argument('-T', '--testing', action='store_true', default=False,
                        help="Run in testing mode: do not update gitlab labels")
    parser.add_argument('--sentry-ca-certs', default=os.environ.get('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')
    return parser.parse_args()


def main(session, args):
    """Find open MRs, check for relevant upstream Fixes."""
    if not args.linux_src:
        LOGGER.warning("No valid Linux source git found, aborting!")
        return

    LOGGER.info("Fetching latest upstream git commits...")
    kgit.pull_remote_branch(args.linux_src, 'origin', 'master')
    try:
        kgit.fetch_all(args.linux_src)
    except subprocess.CalledProcessError:
        pass

    LOGGER.info('Finding open MRs in: %s...', args.projects)
    gl_instance = get_instance(defs.GITFORGE)

    mrs_to_check = {}
    for project in args.projects:
        mrs_to_check.update(get_open_mrs(session, project))

    LOGGER.debug("MR lists: %s", mrs_to_check)
    if not mrs_to_check:
        LOGGER.info('No open MRs to process.')
        return

    gl_projects = {}
    for namespace, mrs in mrs_to_check.items():
        if not (gl_project := gl_projects.get(namespace)):
            gl_project = gl_instance.projects.get(namespace)
            gl_projects[namespace] = gl_project
        for fixes_mr in mrs:
            fixes_mr.check_for_fixes()
            fixes_mr.report_results(only_if_changed=True)


if __name__ == '__main__':
    pargs = _get_parser_args()
    misc.sentry_init(sentry_sdk, ca_certs=pargs.sentry_ca_certs)
    main(SessionRunner.new('fixes', args=pargs), pargs)

#!/usr/bin/env python
"""Look at linux-cve-announce for files touched by a given CVE."""

from argparse import ArgumentParser
from argparse import Namespace
import os
import re
import typing

from cki_lib import logger
from cki_lib import misc
from cki_lib.owners import Subsystem
# GitPython
import git
import sentry_sdk

from webhook import kgit
from webhook.common import get_owners_parser
from webhook.defs import JIRA_BOT_ASSIGNEES
from webhook.defs import JIStatus
from webhook.defs import JiraField
from webhook.libjira import fetch_issues
from webhook.rhissue import RHIssue
from webhook.rhissue import make_rhissues

if typing.TYPE_CHECKING:
    from jira import JIRA
    from jira.resources import Issue

LOGGER = logger.get_logger('webhook.utils.cve_assigner')


def _get_parser_args():
    parser = ArgumentParser(description='Try to figure out default component for a given CVE')

    # Global options
    parser.add_argument('-c', '--cve', help='CVE to look up in linux-cve-announce.', required=True)
    parser.add_argument('-p', '--public-inbox', default=os.environ.get('PUBLIC_INBOX', ''),
                        help='Directory containing linux-cve-announce public inbox git')
    parser.add_argument('-o', '--owners-yaml', default=os.environ.get('OWNERS_YAML', ''),
                        help='Path to the owners.yaml file')
    parser.add_argument('--sentry-ca-certs', default=os.environ.get('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')
    return parser.parse_args()


def get_patch_data(args: 'Namespace', commit: 'git.Commit') -> (list[str], str):
    """Get the file list and upstream commit ID from the linux-cve-announce email."""
    mbox_path = os.path.realpath(f'{args.public_inbox}/m')

    kgit.branch_copy(args.public_inbox, args.cve)
    kgit.checkout(args.public_inbox, f'{args.cve}')
    kgit.hard_reset(args.public_inbox, commit.hexsha)

    with open(mbox_path, 'r', encoding='utf-8') as mbox:
        mbox_data = mbox.read().rstrip()

    LOGGER.debug("Got mbox data of:\n%s", mbox_data)

    save_line = False
    file_paths = []
    ucid = None
    fixpat = (r"^\s*.*[Ff]ixed in (?P<kver2>\d{1,2}\.\d{1,2}(-rc\d{1,2})?) "
              r"with commit (?P<fixsha>[a-f0-9]{12,40})$")
    fix_regex = re.compile(fixpat)

    for line in mbox_data.splitlines():
        fixref = fix_regex.match(line)
        if fixref:
            # Only capture the very last one, which will be the mainline upstream commit ID
            ucid = fixref.group('fixsha')
            continue
        if line.startswith("The file(s) affected by this issue are:"):
            save_line = True
            continue
        if line.startswith("Mitigation"):
            save_line = False
            break
        if save_line and line != '':
            file_paths.append(line.strip())

    return file_paths, ucid


def restore_git_state(args: 'Namespace') -> None:
    """Clean up the git repo state."""
    kgit.checkout(args.public_inbox, 'master')
    kgit.branch_delete(args.public_inbox, args.cve)


def search_for_cve_info(repo: 'git.Repo', cve: str) -> ('git.Commit', bool):
    """Search for the given CVE in the public inbox git."""
    log_output = repo.git.log('--oneline', f'--grep={cve}')
    rejected = False
    commit = None
    for line in log_output.splitlines():
        commit_sha = line.split()[0]
        commit_subj = ' '.join(line.split()[1:])
        if commit_subj.startswith(f'REJECTED: {cve}'):
            rejected = True
        elif commit_subj.startswith(f'{cve}'):
            commit = repo.commit(commit_sha)

    return commit, rejected


def parse_jira_data(args: 'Namespace', sst: Subsystem) -> list[RHIssue]:
    """Get jira data for the given CVE."""
    jira_data = fetch_issues([args.cve], filter_kwf=True)
    if not jira_data:
        LOGGER.info("No jira issues found for %s", args.cve)
        return []

    rhissues = make_rhissues([i.key for i in jira_data], None)

    LOGGER.debug("Unfiltered jira issues for CVE: %s", rhissues)

    rhissues_to_update = []
    for rhi in rhissues:
        if rhi.ji_status == JIStatus.CLOSED:
            LOGGER.debug("Issue %s is Closed", rhi.id)
            continue
        # We need the raw component field to see the full component/sub-component to match against
        new_component = sst.jira_component
        if rhi.ji_component == "kernel-rt":
            new_component = new_component.replace("kernel", "kernel-rt")
        if rhi.component_full != new_component:
            LOGGER.info("Issue %s component mismatch. Jira: '%s' != Docs: '%s'",
                        rhi.id, rhi.component_full, new_component)
            rhissues_to_update.append(rhi)
        else:
            LOGGER.debug("Issue %s components match: '%s'", rhi.id, new_component)

    LOGGER.info("Issues to update: %s", [rhi.id for rhi in rhissues_to_update])

    return rhissues_to_update


def get_valid_components(prefix: str, jira: 'JIRA') -> list[str]:
    """Get the list of valid jira components with the given prefix."""
    all_components = jira.project_components('RHEL')
    return [comp.name for comp in all_components if comp.name.startswith(prefix)]


def _update_jira_issue(
    issue: 'Issue',
    fields: dict[str, typing.Any],
    assignee: typing.Union[str, None],
    update: dict[str, typing.Any]
) -> None:
    """Actually send the update data for the jira issue."""
    LOGGER.debug("_update_jira_issue called for issue %s with fields: %s, assignee: %s, update: %s",
                 issue.id, fields, assignee, update)
    if not misc.is_production_or_staging():
        return

    if assignee:
        issue.update(fields=fields, assignee={'name': assignee}, update=update)
    else:
        issue.update(fields=fields, update=update)


def update_jira_data(rhissues: list[RHIssue], sst: Subsystem, ucid: str) -> None:
    """Set the component to sst.jira_component for the given issues, add upstream commit ID."""
    # Checkbox IDs in the Reset_Contacts field.
    # pme-bot will reset "checked" fields after component reassignment.
    reset_field = {
        'assignee': {"id": "32051"}, 'qa_contact': {"id": "32052"}, 'doc_contact': {"id": "32053"},
        'pool_team': {"id": "32054"}, 'watchers': {"id": "32055"}, 'developer': {"id": "32850"}
    }
    update_all = {JiraField.Reset_Contacts: [{'set': list(reset_field.values())}]}

    valid_rt_components = get_valid_components('kernel-rt', rhissues[0].jira)
    for rhi in rhissues:
        if not rhi.ji_branch:
            LOGGER.warning("Issue %s (FV: %s) is for an unknown branch, refusing to update",
                           rhi.id, rhi.ji_fix_version)
            continue
        fields = {}
        new_component = sst.jira_component
        if rhi.ji_component == "kernel-rt":
            new_component = new_component.replace("kernel", "kernel-rt")
            if new_component not in valid_rt_components:
                LOGGER.warning("Component %s is not present in jira, falling back to %s",
                               new_component, rhi.component_full)
                new_component = rhi.component_full
        # Assigning a more specific component gets it onto an SST's radar sooner. We base this
        # off of owners.yaml, with this minor tweak for kernel-rt clones, so they also get more
        # accurate default RT QE assignees
        if new_component != rhi.component_full:
            fields[JiraField.components] = [{"name": new_component}]
            LOGGER.info("Changing issue %s component from %s to %s",
                        rhi.id, rhi.component_full, new_component)
        is_lead_stream = rhi.ji_branch.lead_stream or rhi.ji_branch.ystream
        # For lead streams, drop in the upstream commit hash(es) that fix the CVE, which
        # will be leveraged by auto-backport tooling to try MR creation ASAP.
        if rhi.ji_component == "kernel" and is_lead_stream:
            fields[JiraField.Commit_Hashes] = ucid
            LOGGER.info("Setting issue %s commit hashes to %s", rhi.id, ucid)

        if rhi.ji_fix_version and not is_lead_stream:
            if rhi.assignee.emailAddress not in JIRA_BOT_ASSIGNEES:
                LOGGER.info("Issue already assigned to non-bot user: %s, leaving assignee as-is",
                            rhi.assignee.displayName)
                resets = [reset_field['qa_contact'], reset_field['doc_contact'],
                          reset_field['watchers'], reset_field['developer']]
                update_already_assigned = {JiraField.Reset_Contacts: [{'set': resets}]}
                _update_jira_issue(rhi.ji, fields, None, update_already_assigned)
                continue
            LOGGER.info("Setting issue %s assignee to se-kernel", rhi.id)
            assignee = 'se-kernel'
            sst_se_kernel = '36050'  # the pool team ID number within jira
            fields[JiraField.Pool_Team] = [{'id': sst_se_kernel}]
            fields[JiraField.QA_Contact] = {'name': 'se-kernel'}
            fields[JiraField.Sub_System_Group] = []
            resets = [reset_field['doc_contact'], reset_field['watchers'], reset_field['developer']]
            update_for_se = {JiraField.Reset_Contacts: [{'set': resets}]}
            _update_jira_issue(rhi.ji, fields, assignee, update_for_se)
            continue
        _update_jira_issue(rhi.ji, fields, None, update_all)


def find_best_matching_component(
    subsystems: list[Subsystem],
) -> typing.Union[Subsystem, None]:
    """Find the best matching subsystem component for file path(s)."""
    if not subsystems:
        return None

    # Match the most detailed (sub-)component, matches "kernel / foo / bar" over "kernel / foo"
    subsystem = subsystems[0]
    for sst in subsystems[1:]:
        if len(sst.jira_component.split("/")) > len(subsystem.jira_component.split("/")):
            subsystem = sst

    return subsystem


def main(args: 'Namespace') -> None:
    """Find open MRs, check for relevant upstream Fixes."""
    if not args.public_inbox:
        LOGGER.warning("No valid public inbox git found, aborting!")
        return

    if not args.cve:
        LOGGER.warning("No valid CVE provided, aborting!")
        return

    LOGGER.debug("Fetching latest upstream mbox data...")
    kgit.checkout(args.public_inbox, 'master')
    kgit.fetch_all(args.public_inbox)
    kgit.hard_reset(args.public_inbox, 'origin/master')
    repo = git.Repo(args.public_inbox)

    commit, rejected = search_for_cve_info(repo, args.cve)

    if rejected:
        LOGGER.warning("%s was REJECTED upstream", args.cve)

    if commit is None:
        LOGGER.warning("No commit found, exiting.")
        return

    LOGGER.debug("Entry:\ncommit: %s\nsubject: %s", commit, commit.summary)

    paths, ucid = get_patch_data(args, commit)
    LOGGER.info("Got upstream commit of %s, path(s) of %s", ucid, paths)
    restore_git_state(args)

    owners = get_owners_parser(args.owners_yaml)
    subsystems = owners.get_matching_subsystems(paths)

    if not (subsystem := find_best_matching_component(subsystems)):
        LOGGER.warning("Unable to find a matching subsystem for paths %s in %s",
                       paths, args.owners_yaml)
        return

    LOGGER.debug("Got subsystem of %s for path(s) %s", subsystem, paths)
    LOGGER.debug("Got UCID of %s", ucid)
    LOGGER.debug("%s's SST: %s, Jira Component mapping: '%s'",
                 subsystem, subsystem.devel_sst, subsystem.jira_component)

    if issues_to_update := parse_jira_data(args, subsystem):
        update_jira_data(issues_to_update, subsystem, ucid)


if __name__ == '__main__':
    pargs = _get_parser_args()
    misc.sentry_init(sentry_sdk, ca_certs=pargs.sentry_ca_certs)
    main(pargs)

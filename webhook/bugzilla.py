"""Bugzilla module."""
import typing

from bugzilla.exceptions import BugzillaError
from cki_lib.logger import get_logger

if typing.TYPE_CHECKING:
    from bugzilla import Bugzilla
    from bugzilla.bug import Bug

LOGGER = get_logger('cki.webhook.bugzilla')


def get_bugs(bugzilla: 'Bugzilla', bug_ids: typing.Iterable[int | str]) -> list['Bug']:
    """Return a list of bugzilla.Bug objects for the given bug_ids."""
    if not bug_ids:
        raise ValueError('bug_ids cannot be empty.')

    LOGGER.info('Querying %s for bug_ids: %s', bugzilla.url, list(bug_ids))

    try:
        bug_data = bugzilla.getbugs(bug_ids, include_fields=['_all'], exclude_fields=['comments'])
    except BugzillaError as err:
        # code 100: 'not a valid bug number nor an alias to a bug'
        # code 101: 'does not exist'
        if err.code not in (100, 101):
            raise
        bug_data = []

    # In theory getbugs() can include None values but I've never seen it happen.
    bug_data: list['Bug'] = [bug for bug in bug_data if bug is not None]

    LOGGER.debug('query returned: %s', [bug.id for bug in bug_data])
    return bug_data


def get_bug(bugzilla: 'Bugzilla', bug_id: int | str) -> 'Bug | None':
    """Return the bugzilla.Bug object matching the input bug_id, or None."""
    bug_data = get_bugs(bugzilla, [bug_id])

    # If the bug_id is a number then match it to the bug.id.
    if isinstance(bug_id, int) or bug_id.isdigit():
        return next((bug for bug in bug_data if bug.id == int(bug_id)), None)

    # If the bug_id is not a number then try to match it to an alias.
    return next((bug for bug in bug_data if bug_id in getattr(bug, 'alias', [])), None)

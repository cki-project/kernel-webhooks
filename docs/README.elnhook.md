# ELN Webhook (elnhook)

## Purpose

This webhook monitors for RHEL kernel configuration changes in the kernel-ark
project.

At this time, that means opening Jira issues when a bot created config MR is
opened during the upstream merge window and linking it to the MR. Opening Jira
issues will hopefully make these review tasks more trackable by the SSTs.

For context, these new configs are initially added to the
`redhat/configs/pending-rhel` directory and a merge request is opened that
moves them to the rhel config directory. This gives the maintainers a chance to
review the config setting before committing it officially to
`redhat/configs/rhel`.

In the future, we plan to expand this hook's responsibilities and potentially
take over some of the config functionality currently maintained in the
kernel-ark project itself. Issue #770 provides some additional background.

## Testing

Define the following in your `.envrc` file:

```bash
export JIRA_TOKEN_AUTH=$(cat your-jira.key)
export COM_GITLAB_TOKEN=$(cat your-gitlab.key)
export GITLAB_TOKENS='{"gitlab.com": "COM_GITLAB_TOKEN"}'
export CKI_LOGGING_LEVEL=DEBUG
export CKI_DEPLOYMENT_ENVIRONMENT=development
export OWNERS_YAML=/path/to/documentation/info/owners.yaml
export REPO_PATH=/path/to/kernel-ark
```

Test the hook locally:

```bash
python3 -m webhook.elnhook --merge-request <gitlab-mr-link>
```

Currently the push event handler is commented out since it is a WIP but to test
a push event run this locally:

```bash
python3 -m webhook.elnhook --json-message-file push-event.json
```
